﻿Shader "Custom/NewTryForCel"
{
    Properties
    {
		[PerRendererData] _MainTex("MainTexture", 2D) = "white"{}
		_NormalMap("NormalMap", 2D) = "white" {}
		_Color("Color", color) = (1,1,1,1) 

		_RimColor("Rim Color", color) = (1,1,1,1) 
		_RimPower("Rim Power", Range(0.5, 8.0)) = 3.0

		_ShadowBrightness("Shadow Brightness", Range(0, 0.69)) = 0.3
		_HighlightBrightness("Highlight Brightness", Range(0, 0.3)) = 0.1
			
	}
		SubShader
		{
			Tags{
				 "Queue" = "Transparent"
				 "RenderType" = "Fade"
				"IgnoreProjector" = "false"
				"CanUseSpriteAtlas" = "true"
			}
			Cull off
			Lighting On
			Zwrite Off
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

			#pragma surface surf CustomLight alpha vertex:ver

			#pragma target 3.0

		    static float lightDirX;

			float _HighlightBrightness;
			float _ShadowBrightness;

			half4 LightingCustomLight(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {

				half ndotl = dot(s.Normal, lightDir);

				half4 l;			


				if (ndotl > 0.95)
					l.rgb = float4(0.7 + _HighlightBrightness, 0.7 + _HighlightBrightness, 0.7 + _HighlightBrightness, 1) * _LightColor0.rgb * atten * s.Albedo;
				else if (ndotl > 0.5)
					l.rgb = float4(0.7, 0.7, 0.7, 1) * _LightColor0.rgb * atten  * s.Albedo;
				else if (ndotl > 0.05)
					l.rgb = float4(0 + _ShadowBrightness, 0 + _ShadowBrightness, 0 + _ShadowBrightness, 1) * _LightColor0.rgb * atten  * s.Albedo;

				l.a = s.Alpha;					
				
				return l;
			}		

			void old() {

			}

			sampler2D _MainTex;
			sampler2D _NormalMap;

			struct Input {

				float2 uv_MainTex; 
				fixed4 color;			
			};

		
			fixed4 _Color;

			void ver(inout appdata_full v, out Input o) {

				v.normal = float3(0, 0, -1);			

				v.tangent = float4(1, 0, 0, 1);

				UNITY_INITIALIZE_OUTPUT(Input, o);

				o.color = _Color * v.color;
			}

			
			void surf(Input IN, inout SurfaceOutput o) {

				fixed4 t = tex2D(_MainTex, IN.uv_MainTex);
				o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));

				o.Albedo = t.rgb * _Color;
				o.Alpha = t.a;
			}

		 ENDCG
    }
    FallBack "Diffuse"
}
