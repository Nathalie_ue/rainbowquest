﻿Shader "Unlit Master"
{
	Properties
	{
		[NoScaleOffset]_MainTex("MainTex", 2D) = "white" {}
		_Color("Color", Color) = (1, 1, 1, 0)
		_Position("PlayerPosition", Vector) = (0.5, 0.5, 0, 0)
		_Size("Size", Float) = 1
		_Smoothness("Smoothness", Range(0, 1)) = 0
		_Opacity("Opacity", Range(0, 1)) = 0
		_NoiseScale("NoiseScale", Range(0, 100)) = 10
		_Offset("NoiseOffset", Float) = 0
	}
		SubShader
		{
			Tags
			{
				"RenderType" = "Transparent"
				"Queue" = "Transparent+0"
			}

			Pass
			{
				Name "Pass"
				Tags
				{
			// LightMode: <None>
		}

		// Render State
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		Cull Back
		ZTest LEqual
		ZWrite Off
			// ColorMask: <None>


			HLSLPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			// Debug
			// <None>

			// --------------------------------------------------
			// Pass

			// Pragmas
			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x
			#pragma target 2.0
			#pragma multi_compile_fog
			#pragma multi_compile_instancing

			// Keywords
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma shader_feature _ _SAMPLE_GI
			// GraphKeywords: <None>

			// Defines
			#define _SURFACE_TYPE_TRANSPARENT 1
			#define ATTRIBUTES_NEED_NORMAL
			#define ATTRIBUTES_NEED_TANGENT
			#define ATTRIBUTES_NEED_TEXCOORD0
			#define VARYINGS_NEED_POSITION_WS 
			#define VARYINGS_NEED_TEXCOORD0
			#define SHADERPASS_UNLIT

			// Includes
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

			// --------------------------------------------------
			// Graph

			// Graph Properties
			CBUFFER_START(UnityPerMaterial)
			float4 _Color;
			float2 _Position;
			float _Size;
			float _Smoothness;
			float _Opacity;
			float _NoiseScale;
			float _Offset;
			CBUFFER_END
			TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
			SAMPLER(_SampleTexture2D_7639D566_Sampler_3_Linear_Repeat);

			// Graph Functions

			void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
			{
				Out = A * B;
			}

			void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
			{
				Out = UV * Tiling + Offset;
			}


			float2 Unity_GradientNoise_Dir_float(float2 p)
			{
				// Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
				p = p % 289;
				float x = (34 * p.x + 1) * p.x % 289 + p.y;
				x = (34 * x + 1) * x % 289;
				x = frac(x / 41) * 2 - 1;
				return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
			}

			void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
			{
				float2 p = UV * Scale;
				float2 ip = floor(p);
				float2 fp = frac(p);
				float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
				float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
				float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
				float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
				fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
				Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
			}

			void Unity_Remap_float2(float2 In, float2 InMinMax, float2 OutMinMax, out float2 Out)
			{
				Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
			}

			void Unity_Add_float2(float2 A, float2 B, out float2 Out)
			{
				Out = A + B;
			}

			void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
			{
				Out = A * B;
			}

			void Unity_Subtract_float2(float2 A, float2 B, out float2 Out)
			{
				Out = A - B;
			}

			void Unity_Divide_float(float A, float B, out float Out)
			{
				Out = A / B;
			}

			void Unity_Multiply_float(float A, float B, out float Out)
			{
				Out = A * B;
			}

			void Unity_Divide_float2(float2 A, float2 B, out float2 Out)
			{
				Out = A / B;
			}

			void Unity_Length_float2(float2 In, out float Out)
			{
				Out = length(In);
			}

			void Unity_OneMinus_float(float In, out float Out)
			{
				Out = 1 - In;
			}

			void Unity_Saturate_float(float In, out float Out)
			{
				Out = saturate(In);
			}

			void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
			{
				Out = smoothstep(Edge1, Edge2, In);
			}

			void Unity_Add_float(float A, float B, out float Out)
			{
				Out = A + B;
			}

			void Unity_Clamp_float(float In, float Min, float Max, out float Out)
			{
				Out = clamp(In, Min, Max);
			}

			// Graph Vertex
			// GraphVertex: <None>

			// Graph Pixel
			struct SurfaceDescriptionInputs
			{
				float3 WorldSpacePosition;
				float4 ScreenPosition;
				float4 uv0;
			};

			struct SurfaceDescription
			{
				float3 Color;
				float Alpha;
				float AlphaClipThreshold;
			};

			SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
			{
				SurfaceDescription surface = (SurfaceDescription)0;
				float4 _Property_E5D92E37_Out_0 = _Color;
				float4 _SampleTexture2D_7639D566_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, IN.uv0.xy);
				float _SampleTexture2D_7639D566_R_4 = _SampleTexture2D_7639D566_RGBA_0.r;
				float _SampleTexture2D_7639D566_G_5 = _SampleTexture2D_7639D566_RGBA_0.g;
				float _SampleTexture2D_7639D566_B_6 = _SampleTexture2D_7639D566_RGBA_0.b;
				float _SampleTexture2D_7639D566_A_7 = _SampleTexture2D_7639D566_RGBA_0.a;
				float4 _Multiply_D82A1732_Out_2;
				Unity_Multiply_float(_Property_E5D92E37_Out_0, _SampleTexture2D_7639D566_RGBA_0, _Multiply_D82A1732_Out_2);
				float _Property_65D873D9_Out_0 = _Offset;
				float2 _TilingAndOffset_49705D52_Out_3;
				Unity_TilingAndOffset_float((IN.WorldSpacePosition.xy), float2 (1, 1), (_Property_65D873D9_Out_0.xx), _TilingAndOffset_49705D52_Out_3);
				float _Property_24E49D68_Out_0 = _NoiseScale;
				float _GradientNoise_CD1FA78B_Out_2;
				Unity_GradientNoise_float(_TilingAndOffset_49705D52_Out_3, _Property_24E49D68_Out_0, _GradientNoise_CD1FA78B_Out_2);
				float _Property_108F68D_Out_0 = _Smoothness;
				float4 _ScreenPosition_D18C4FE8_Out_0 = float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0);
				float2 _Property_8D4B551E_Out_0 = _Position;
				float2 _Remap_7261D8E1_Out_3;
				Unity_Remap_float2(_Property_8D4B551E_Out_0, float2 (0, 1), float2 (0.5, -1.5), _Remap_7261D8E1_Out_3);
				float2 _Add_2B445881_Out_2;
				Unity_Add_float2((_ScreenPosition_D18C4FE8_Out_0.xy), _Remap_7261D8E1_Out_3, _Add_2B445881_Out_2);
				float2 _TilingAndOffset_9ADD6887_Out_3;
				Unity_TilingAndOffset_float((_ScreenPosition_D18C4FE8_Out_0.xy), float2 (1, 1), _Add_2B445881_Out_2, _TilingAndOffset_9ADD6887_Out_3);
				float2 _Multiply_4C98F91B_Out_2;
				Unity_Multiply_float(_TilingAndOffset_9ADD6887_Out_3, float2(2, 2), _Multiply_4C98F91B_Out_2);
				float2 _Subtract_73F461ED_Out_2;
				Unity_Subtract_float2(_Multiply_4C98F91B_Out_2, float2(1, 1), _Subtract_73F461ED_Out_2);
				float _Divide_80A129F_Out_2;
				Unity_Divide_float(unity_OrthoParams.y, unity_OrthoParams.x, _Divide_80A129F_Out_2);
				float _Property_6B992450_Out_0 = _Size;
				float _Multiply_849DEF79_Out_2;
				Unity_Multiply_float(_Divide_80A129F_Out_2, _Property_6B992450_Out_0, _Multiply_849DEF79_Out_2);
				float2 _Vector2_85763613_Out_0 = float2(_Multiply_849DEF79_Out_2, _Property_6B992450_Out_0);
				float2 _Divide_CEB0A768_Out_2;
				Unity_Divide_float2(_Subtract_73F461ED_Out_2, _Vector2_85763613_Out_0, _Divide_CEB0A768_Out_2);
				float _Length_B2E50CD2_Out_1;
				Unity_Length_float2(_Divide_CEB0A768_Out_2, _Length_B2E50CD2_Out_1);
				float _OneMinus_34D29F9C_Out_1;
				Unity_OneMinus_float(_Length_B2E50CD2_Out_1, _OneMinus_34D29F9C_Out_1);
				float _Saturate_F0673799_Out_1;
				Unity_Saturate_float(_OneMinus_34D29F9C_Out_1, _Saturate_F0673799_Out_1);
				float _Smoothstep_C3ECB3E2_Out_3;
				Unity_Smoothstep_float(0, _Property_108F68D_Out_0, _Saturate_F0673799_Out_1, _Smoothstep_C3ECB3E2_Out_3);
				float _Multiply_B9668E20_Out_2;
				Unity_Multiply_float(_GradientNoise_CD1FA78B_Out_2, _Smoothstep_C3ECB3E2_Out_3, _Multiply_B9668E20_Out_2);
				float _Add_AB87D008_Out_2;
				Unity_Add_float(_Multiply_B9668E20_Out_2, _Smoothstep_C3ECB3E2_Out_3, _Add_AB87D008_Out_2);
				float _Property_F02E3119_Out_0 = _Opacity;
				float _Multiply_2002AC69_Out_2;
				Unity_Multiply_float(_Add_AB87D008_Out_2, _Property_F02E3119_Out_0, _Multiply_2002AC69_Out_2);
				float _Clamp_E1AE3BBF_Out_3;
				Unity_Clamp_float(_Multiply_2002AC69_Out_2, 0, 1, _Clamp_E1AE3BBF_Out_3);
				float _OneMinus_63A1FA28_Out_1;
				Unity_OneMinus_float(_Clamp_E1AE3BBF_Out_3, _OneMinus_63A1FA28_Out_1);
				surface.Color = (_Multiply_D82A1732_Out_2.xyz);
				surface.Alpha = _OneMinus_63A1FA28_Out_1;
				surface.AlphaClipThreshold = 0;
				return surface;
			}

			// --------------------------------------------------
			// Structs and Packing

			// Generated Type: Attributes
			struct Attributes
			{
				float3 positionOS : POSITION;
				float3 normalOS : NORMAL;
				float4 tangentOS : TANGENT;
				float4 uv0 : TEXCOORD0;
				#if UNITY_ANY_INSTANCING_ENABLED
				uint instanceID : INSTANCEID_SEMANTIC;
				#endif
			};

			// Generated Type: Varyings
			struct Varyings
			{
				float4 positionCS : SV_POSITION;
				float3 positionWS;
				float4 texCoord0;
				#if UNITY_ANY_INSTANCING_ENABLED
				uint instanceID : CUSTOM_INSTANCE_ID;
				#endif
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
				#endif
			};

			// Generated Type: PackedVaryings
			struct PackedVaryings
			{
				float4 positionCS : SV_POSITION;
				#if UNITY_ANY_INSTANCING_ENABLED
				uint instanceID : CUSTOM_INSTANCE_ID;
				#endif
				float3 interp00 : TEXCOORD0;
				float4 interp01 : TEXCOORD1;
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
				#endif
			};

			// Packed Type: Varyings
			PackedVaryings PackVaryings(Varyings input)
			{
				PackedVaryings output = (PackedVaryings)0;
				output.positionCS = input.positionCS;
				output.interp00.xyz = input.positionWS;
				output.interp01.xyzw = input.texCoord0;
				#if UNITY_ANY_INSTANCING_ENABLED
				output.instanceID = input.instanceID;
				#endif
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				output.cullFace = input.cullFace;
				#endif
				return output;
			}

			// Unpacked Type: Varyings
			Varyings UnpackVaryings(PackedVaryings input)
			{
				Varyings output = (Varyings)0;
				output.positionCS = input.positionCS;
				output.positionWS = input.interp00.xyz;
				output.texCoord0 = input.interp01.xyzw;
				#if UNITY_ANY_INSTANCING_ENABLED
				output.instanceID = input.instanceID;
				#endif
				#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
				output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
				#endif
				#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
				output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
				#endif
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				output.cullFace = input.cullFace;
				#endif
				return output;
			}

			// --------------------------------------------------
			// Build Graph Inputs

			SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
			{
				SurfaceDescriptionInputs output;
				ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





				output.WorldSpacePosition = input.positionWS;
				output.ScreenPosition = ComputeScreenPos(TransformWorldToHClip(input.positionWS), _ProjectionParams.x);
				output.uv0 = input.texCoord0;
			#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
			#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
			#else
			#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
			#endif
			#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

				return output;
			}


			// --------------------------------------------------
			// Main

			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/UnlitPass.hlsl"

			ENDHLSL
		}

		Pass
		{
			Name "ShadowCaster"
			Tags
			{
				"LightMode" = "ShadowCaster"
			}

				// Render State
				Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
				Cull Back
				ZTest LEqual
				ZWrite On
				// ColorMask: <None>


				HLSLPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				// Debug
				// <None>

				// --------------------------------------------------
				// Pass

				// Pragmas
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x
				#pragma target 2.0
				#pragma multi_compile_instancing

				// Keywords
				#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
				// GraphKeywords: <None>

				// Defines
				#define _SURFACE_TYPE_TRANSPARENT 1
				#define ATTRIBUTES_NEED_NORMAL
				#define ATTRIBUTES_NEED_TANGENT
				#define VARYINGS_NEED_POSITION_WS 
				#define SHADERPASS_SHADOWCASTER

				// Includes
				#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
				#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

				// --------------------------------------------------
				// Graph

				// Graph Properties
				CBUFFER_START(UnityPerMaterial)
				float4 _Color;
				float2 _Position;
				float _Size;
				float _Smoothness;
				float _Opacity;
				float _NoiseScale;
				float _Offset;
				CBUFFER_END
				TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;

				// Graph Functions

				void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
				{
					Out = UV * Tiling + Offset;
				}


				float2 Unity_GradientNoise_Dir_float(float2 p)
				{
					// Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
					p = p % 289;
					float x = (34 * p.x + 1) * p.x % 289 + p.y;
					x = (34 * x + 1) * x % 289;
					x = frac(x / 41) * 2 - 1;
					return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
				}

				void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
				{
					float2 p = UV * Scale;
					float2 ip = floor(p);
					float2 fp = frac(p);
					float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
					float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
					float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
					float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
					fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
					Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
				}

				void Unity_Remap_float2(float2 In, float2 InMinMax, float2 OutMinMax, out float2 Out)
				{
					Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
				}

				void Unity_Add_float2(float2 A, float2 B, out float2 Out)
				{
					Out = A + B;
				}

				void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
				{
					Out = A * B;
				}

				void Unity_Subtract_float2(float2 A, float2 B, out float2 Out)
				{
					Out = A - B;
				}

				void Unity_Divide_float(float A, float B, out float Out)
				{
					Out = A / B;
				}

				void Unity_Multiply_float(float A, float B, out float Out)
				{
					Out = A * B;
				}

				void Unity_Divide_float2(float2 A, float2 B, out float2 Out)
				{
					Out = A / B;
				}

				void Unity_Length_float2(float2 In, out float Out)
				{
					Out = length(In);
				}

				void Unity_OneMinus_float(float In, out float Out)
				{
					Out = 1 - In;
				}

				void Unity_Saturate_float(float In, out float Out)
				{
					Out = saturate(In);
				}

				void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
				{
					Out = smoothstep(Edge1, Edge2, In);
				}

				void Unity_Add_float(float A, float B, out float Out)
				{
					Out = A + B;
				}

				void Unity_Clamp_float(float In, float Min, float Max, out float Out)
				{
					Out = clamp(In, Min, Max);
				}

				// Graph Vertex
				// GraphVertex: <None>

				// Graph Pixel
				struct SurfaceDescriptionInputs
				{
					float3 WorldSpacePosition;
					float4 ScreenPosition;
				};

				struct SurfaceDescription
				{
					float Alpha;
					float AlphaClipThreshold;
				};

				SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
				{
					SurfaceDescription surface = (SurfaceDescription)0;
					float _Property_65D873D9_Out_0 = _Offset;
					float2 _TilingAndOffset_49705D52_Out_3;
					Unity_TilingAndOffset_float((IN.WorldSpacePosition.xy), float2 (1, 1), (_Property_65D873D9_Out_0.xx), _TilingAndOffset_49705D52_Out_3);
					float _Property_24E49D68_Out_0 = _NoiseScale;
					float _GradientNoise_CD1FA78B_Out_2;
					Unity_GradientNoise_float(_TilingAndOffset_49705D52_Out_3, _Property_24E49D68_Out_0, _GradientNoise_CD1FA78B_Out_2);
					float _Property_108F68D_Out_0 = _Smoothness;
					float4 _ScreenPosition_D18C4FE8_Out_0 = float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0);
					float2 _Property_8D4B551E_Out_0 = _Position;
					float2 _Remap_7261D8E1_Out_3;
					Unity_Remap_float2(_Property_8D4B551E_Out_0, float2 (0, 1), float2 (0.5, -1.5), _Remap_7261D8E1_Out_3);
					float2 _Add_2B445881_Out_2;
					Unity_Add_float2((_ScreenPosition_D18C4FE8_Out_0.xy), _Remap_7261D8E1_Out_3, _Add_2B445881_Out_2);
					float2 _TilingAndOffset_9ADD6887_Out_3;
					Unity_TilingAndOffset_float((_ScreenPosition_D18C4FE8_Out_0.xy), float2 (1, 1), _Add_2B445881_Out_2, _TilingAndOffset_9ADD6887_Out_3);
					float2 _Multiply_4C98F91B_Out_2;
					Unity_Multiply_float(_TilingAndOffset_9ADD6887_Out_3, float2(2, 2), _Multiply_4C98F91B_Out_2);
					float2 _Subtract_73F461ED_Out_2;
					Unity_Subtract_float2(_Multiply_4C98F91B_Out_2, float2(1, 1), _Subtract_73F461ED_Out_2);
					float _Divide_80A129F_Out_2;
					Unity_Divide_float(unity_OrthoParams.y, unity_OrthoParams.x, _Divide_80A129F_Out_2);
					float _Property_6B992450_Out_0 = _Size;
					float _Multiply_849DEF79_Out_2;
					Unity_Multiply_float(_Divide_80A129F_Out_2, _Property_6B992450_Out_0, _Multiply_849DEF79_Out_2);
					float2 _Vector2_85763613_Out_0 = float2(_Multiply_849DEF79_Out_2, _Property_6B992450_Out_0);
					float2 _Divide_CEB0A768_Out_2;
					Unity_Divide_float2(_Subtract_73F461ED_Out_2, _Vector2_85763613_Out_0, _Divide_CEB0A768_Out_2);
					float _Length_B2E50CD2_Out_1;
					Unity_Length_float2(_Divide_CEB0A768_Out_2, _Length_B2E50CD2_Out_1);
					float _OneMinus_34D29F9C_Out_1;
					Unity_OneMinus_float(_Length_B2E50CD2_Out_1, _OneMinus_34D29F9C_Out_1);
					float _Saturate_F0673799_Out_1;
					Unity_Saturate_float(_OneMinus_34D29F9C_Out_1, _Saturate_F0673799_Out_1);
					float _Smoothstep_C3ECB3E2_Out_3;
					Unity_Smoothstep_float(0, _Property_108F68D_Out_0, _Saturate_F0673799_Out_1, _Smoothstep_C3ECB3E2_Out_3);
					float _Multiply_B9668E20_Out_2;
					Unity_Multiply_float(_GradientNoise_CD1FA78B_Out_2, _Smoothstep_C3ECB3E2_Out_3, _Multiply_B9668E20_Out_2);
					float _Add_AB87D008_Out_2;
					Unity_Add_float(_Multiply_B9668E20_Out_2, _Smoothstep_C3ECB3E2_Out_3, _Add_AB87D008_Out_2);
					float _Property_F02E3119_Out_0 = _Opacity;
					float _Multiply_2002AC69_Out_2;
					Unity_Multiply_float(_Add_AB87D008_Out_2, _Property_F02E3119_Out_0, _Multiply_2002AC69_Out_2);
					float _Clamp_E1AE3BBF_Out_3;
					Unity_Clamp_float(_Multiply_2002AC69_Out_2, 0, 1, _Clamp_E1AE3BBF_Out_3);
					float _OneMinus_63A1FA28_Out_1;
					Unity_OneMinus_float(_Clamp_E1AE3BBF_Out_3, _OneMinus_63A1FA28_Out_1);
					surface.Alpha = _OneMinus_63A1FA28_Out_1;
					surface.AlphaClipThreshold = 0;
					return surface;
				}

				// --------------------------------------------------
				// Structs and Packing

				// Generated Type: Attributes
				struct Attributes
				{
					float3 positionOS : POSITION;
					float3 normalOS : NORMAL;
					float4 tangentOS : TANGENT;
					#if UNITY_ANY_INSTANCING_ENABLED
					uint instanceID : INSTANCEID_SEMANTIC;
					#endif
				};

				// Generated Type: Varyings
				struct Varyings
				{
					float4 positionCS : SV_POSITION;
					float3 positionWS;
					#if UNITY_ANY_INSTANCING_ENABLED
					uint instanceID : CUSTOM_INSTANCE_ID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
					#endif
				};

				// Generated Type: PackedVaryings
				struct PackedVaryings
				{
					float4 positionCS : SV_POSITION;
					#if UNITY_ANY_INSTANCING_ENABLED
					uint instanceID : CUSTOM_INSTANCE_ID;
					#endif
					float3 interp00 : TEXCOORD0;
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
					#endif
				};

				// Packed Type: Varyings
				PackedVaryings PackVaryings(Varyings input)
				{
					PackedVaryings output = (PackedVaryings)0;
					output.positionCS = input.positionCS;
					output.interp00.xyz = input.positionWS;
					#if UNITY_ANY_INSTANCING_ENABLED
					output.instanceID = input.instanceID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					output.cullFace = input.cullFace;
					#endif
					return output;
				}

				// Unpacked Type: Varyings
				Varyings UnpackVaryings(PackedVaryings input)
				{
					Varyings output = (Varyings)0;
					output.positionCS = input.positionCS;
					output.positionWS = input.interp00.xyz;
					#if UNITY_ANY_INSTANCING_ENABLED
					output.instanceID = input.instanceID;
					#endif
					#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
					output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
					#endif
					#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
					output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
					#endif
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					output.cullFace = input.cullFace;
					#endif
					return output;
				}

				// --------------------------------------------------
				// Build Graph Inputs

				SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
				{
					SurfaceDescriptionInputs output;
					ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





					output.WorldSpacePosition = input.positionWS;
					output.ScreenPosition = ComputeScreenPos(TransformWorldToHClip(input.positionWS), _ProjectionParams.x);
				#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
				#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
				#else
				#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
				#endif
				#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

					return output;
				}


				// --------------------------------------------------
				// Main

				#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
				#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

				ENDHLSL
			}

			Pass
			{
				Name "DepthOnly"
				Tags
				{
					"LightMode" = "DepthOnly"
				}

					// Render State
					Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
					Cull Back
					ZTest LEqual
					ZWrite On
					ColorMask 0


					HLSLPROGRAM
					#pragma vertex vert
					#pragma fragment frag

					// Debug
					// <None>

					// --------------------------------------------------
					// Pass

					// Pragmas
					#pragma prefer_hlslcc gles
					#pragma exclude_renderers d3d11_9x
					#pragma target 2.0
					#pragma multi_compile_instancing

					// Keywords
					// PassKeywords: <None>
					// GraphKeywords: <None>

					// Defines
					#define _SURFACE_TYPE_TRANSPARENT 1
					#define ATTRIBUTES_NEED_NORMAL
					#define ATTRIBUTES_NEED_TANGENT
					#define VARYINGS_NEED_POSITION_WS 
					#define SHADERPASS_DEPTHONLY

					// Includes
					#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
					#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

					// --------------------------------------------------
					// Graph

					// Graph Properties
					CBUFFER_START(UnityPerMaterial)
					float4 _Color;
					float2 _Position;
					float _Size;
					float _Smoothness;
					float _Opacity;
					float _NoiseScale;
					float _Offset;
					CBUFFER_END
					TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;

					// Graph Functions

					void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
					{
						Out = UV * Tiling + Offset;
					}


					float2 Unity_GradientNoise_Dir_float(float2 p)
					{
						// Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
						p = p % 289;
						float x = (34 * p.x + 1) * p.x % 289 + p.y;
						x = (34 * x + 1) * x % 289;
						x = frac(x / 41) * 2 - 1;
						return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
					}

					void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
					{
						float2 p = UV * Scale;
						float2 ip = floor(p);
						float2 fp = frac(p);
						float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
						float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
						float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
						float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
						fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
						Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
					}

					void Unity_Remap_float2(float2 In, float2 InMinMax, float2 OutMinMax, out float2 Out)
					{
						Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
					}

					void Unity_Add_float2(float2 A, float2 B, out float2 Out)
					{
						Out = A + B;
					}

					void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
					{
						Out = A * B;
					}

					void Unity_Subtract_float2(float2 A, float2 B, out float2 Out)
					{
						Out = A - B;
					}

					void Unity_Divide_float(float A, float B, out float Out)
					{
						Out = A / B;
					}

					void Unity_Multiply_float(float A, float B, out float Out)
					{
						Out = A * B;
					}

					void Unity_Divide_float2(float2 A, float2 B, out float2 Out)
					{
						Out = A / B;
					}

					void Unity_Length_float2(float2 In, out float Out)
					{
						Out = length(In);
					}

					void Unity_OneMinus_float(float In, out float Out)
					{
						Out = 1 - In;
					}

					void Unity_Saturate_float(float In, out float Out)
					{
						Out = saturate(In);
					}

					void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
					{
						Out = smoothstep(Edge1, Edge2, In);
					}

					void Unity_Add_float(float A, float B, out float Out)
					{
						Out = A + B;
					}

					void Unity_Clamp_float(float In, float Min, float Max, out float Out)
					{
						Out = clamp(In, Min, Max);
					}

					// Graph Vertex
					// GraphVertex: <None>

					// Graph Pixel
					struct SurfaceDescriptionInputs
					{
						float3 WorldSpacePosition;
						float4 ScreenPosition;
					};

					struct SurfaceDescription
					{
						float Alpha;
						float AlphaClipThreshold;
					};

					SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
					{
						SurfaceDescription surface = (SurfaceDescription)0;
						float _Property_65D873D9_Out_0 = _Offset;
						float2 _TilingAndOffset_49705D52_Out_3;
						Unity_TilingAndOffset_float((IN.WorldSpacePosition.xy), float2 (1, 1), (_Property_65D873D9_Out_0.xx), _TilingAndOffset_49705D52_Out_3);
						float _Property_24E49D68_Out_0 = _NoiseScale;
						float _GradientNoise_CD1FA78B_Out_2;
						Unity_GradientNoise_float(_TilingAndOffset_49705D52_Out_3, _Property_24E49D68_Out_0, _GradientNoise_CD1FA78B_Out_2);
						float _Property_108F68D_Out_0 = _Smoothness;
						float4 _ScreenPosition_D18C4FE8_Out_0 = float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0);
						float2 _Property_8D4B551E_Out_0 = _Position;
						float2 _Remap_7261D8E1_Out_3;
						Unity_Remap_float2(_Property_8D4B551E_Out_0, float2 (0, 1), float2 (0.5, -1.5), _Remap_7261D8E1_Out_3);
						float2 _Add_2B445881_Out_2;
						Unity_Add_float2((_ScreenPosition_D18C4FE8_Out_0.xy), _Remap_7261D8E1_Out_3, _Add_2B445881_Out_2);
						float2 _TilingAndOffset_9ADD6887_Out_3;
						Unity_TilingAndOffset_float((_ScreenPosition_D18C4FE8_Out_0.xy), float2 (1, 1), _Add_2B445881_Out_2, _TilingAndOffset_9ADD6887_Out_3);
						float2 _Multiply_4C98F91B_Out_2;
						Unity_Multiply_float(_TilingAndOffset_9ADD6887_Out_3, float2(2, 2), _Multiply_4C98F91B_Out_2);
						float2 _Subtract_73F461ED_Out_2;
						Unity_Subtract_float2(_Multiply_4C98F91B_Out_2, float2(1, 1), _Subtract_73F461ED_Out_2);
						float _Divide_80A129F_Out_2;
						Unity_Divide_float(unity_OrthoParams.y, unity_OrthoParams.x, _Divide_80A129F_Out_2);
						float _Property_6B992450_Out_0 = _Size;
						float _Multiply_849DEF79_Out_2;
						Unity_Multiply_float(_Divide_80A129F_Out_2, _Property_6B992450_Out_0, _Multiply_849DEF79_Out_2);
						float2 _Vector2_85763613_Out_0 = float2(_Multiply_849DEF79_Out_2, _Property_6B992450_Out_0);
						float2 _Divide_CEB0A768_Out_2;
						Unity_Divide_float2(_Subtract_73F461ED_Out_2, _Vector2_85763613_Out_0, _Divide_CEB0A768_Out_2);
						float _Length_B2E50CD2_Out_1;
						Unity_Length_float2(_Divide_CEB0A768_Out_2, _Length_B2E50CD2_Out_1);
						float _OneMinus_34D29F9C_Out_1;
						Unity_OneMinus_float(_Length_B2E50CD2_Out_1, _OneMinus_34D29F9C_Out_1);
						float _Saturate_F0673799_Out_1;
						Unity_Saturate_float(_OneMinus_34D29F9C_Out_1, _Saturate_F0673799_Out_1);
						float _Smoothstep_C3ECB3E2_Out_3;
						Unity_Smoothstep_float(0, _Property_108F68D_Out_0, _Saturate_F0673799_Out_1, _Smoothstep_C3ECB3E2_Out_3);
						float _Multiply_B9668E20_Out_2;
						Unity_Multiply_float(_GradientNoise_CD1FA78B_Out_2, _Smoothstep_C3ECB3E2_Out_3, _Multiply_B9668E20_Out_2);
						float _Add_AB87D008_Out_2;
						Unity_Add_float(_Multiply_B9668E20_Out_2, _Smoothstep_C3ECB3E2_Out_3, _Add_AB87D008_Out_2);
						float _Property_F02E3119_Out_0 = _Opacity;
						float _Multiply_2002AC69_Out_2;
						Unity_Multiply_float(_Add_AB87D008_Out_2, _Property_F02E3119_Out_0, _Multiply_2002AC69_Out_2);
						float _Clamp_E1AE3BBF_Out_3;
						Unity_Clamp_float(_Multiply_2002AC69_Out_2, 0, 1, _Clamp_E1AE3BBF_Out_3);
						float _OneMinus_63A1FA28_Out_1;
						Unity_OneMinus_float(_Clamp_E1AE3BBF_Out_3, _OneMinus_63A1FA28_Out_1);
						surface.Alpha = _OneMinus_63A1FA28_Out_1;
						surface.AlphaClipThreshold = 0;
						return surface;
					}

					// --------------------------------------------------
					// Structs and Packing

					// Generated Type: Attributes
					struct Attributes
					{
						float3 positionOS : POSITION;
						float3 normalOS : NORMAL;
						float4 tangentOS : TANGENT;
						#if UNITY_ANY_INSTANCING_ENABLED
						uint instanceID : INSTANCEID_SEMANTIC;
						#endif
					};

					// Generated Type: Varyings
					struct Varyings
					{
						float4 positionCS : SV_POSITION;
						float3 positionWS;
						#if UNITY_ANY_INSTANCING_ENABLED
						uint instanceID : CUSTOM_INSTANCE_ID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
						#endif
					};

					// Generated Type: PackedVaryings
					struct PackedVaryings
					{
						float4 positionCS : SV_POSITION;
						#if UNITY_ANY_INSTANCING_ENABLED
						uint instanceID : CUSTOM_INSTANCE_ID;
						#endif
						float3 interp00 : TEXCOORD0;
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
						#endif
					};

					// Packed Type: Varyings
					PackedVaryings PackVaryings(Varyings input)
					{
						PackedVaryings output = (PackedVaryings)0;
						output.positionCS = input.positionCS;
						output.interp00.xyz = input.positionWS;
						#if UNITY_ANY_INSTANCING_ENABLED
						output.instanceID = input.instanceID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						output.cullFace = input.cullFace;
						#endif
						return output;
					}

					// Unpacked Type: Varyings
					Varyings UnpackVaryings(PackedVaryings input)
					{
						Varyings output = (Varyings)0;
						output.positionCS = input.positionCS;
						output.positionWS = input.interp00.xyz;
						#if UNITY_ANY_INSTANCING_ENABLED
						output.instanceID = input.instanceID;
						#endif
						#if (defined(UNITY_STEREO_INSTANCING_ENABLED))
						output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
						#endif
						#if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
						output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
						#endif
						#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
						output.cullFace = input.cullFace;
						#endif
						return output;
					}

					// --------------------------------------------------
					// Build Graph Inputs

					SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
					{
						SurfaceDescriptionInputs output;
						ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





						output.WorldSpacePosition = input.positionWS;
						output.ScreenPosition = ComputeScreenPos(TransformWorldToHClip(input.positionWS), _ProjectionParams.x);
					#if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
					#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
					#else
					#define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
					#endif
					#undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

						return output;
					}


					// --------------------------------------------------
					// Main

					#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

					ENDHLSL
				}

		}
			FallBack "Hidden/Shader Graph/FallbackError"
}
