﻿Shader "Unlit/MySobel"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DepthCustom ("DepthTexture", 2D) = "white" {}
		_Thickness("Outline thickness", float) = 1
		_DepthStrength("Depth strength", float) = 1
		_DepthThickness("Depth thickness", float) = 1
		_DepthThreshHold("Septh threshhold", float) = 1
		_OutlineColor("Outline Color", color) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			static float2 sobelSamplePoints[9] = {
					float2(-1, 1), float2(0, 1), float2(1, 1),
					float2(-1, 0), float2(0, 0), float2(1, 1),
					float2(-1, 1), float2(0, -1), float2(1, -1),
			};

			static float sobelXMatrix[9] = {
				1, 0, -1,
				2, 0, -2,
				1, 0, -1

			};

			static float sobelYMatrix[9] = {
				1, 2, 1,
				0, 0, 0,
				-1, -2, -1

			};

			void CustomSobel_float(float2 UV, float Thickness, sampler2D tex, out float Out)
			{
				float2 sobel = 0;

				[unroll]
				for (int i = 0; i < 9; i++)
				{
					float depth = tex2D(tex, UV + sobelSamplePoints[i] * Thickness);
					sobel += depth * float2(sobelXMatrix[i], sobelYMatrix[i]);
				}

				Out = length(sobel);
			}

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _DepthCustom;
            float4 _MainTex_ST;
			float _Thickness;
			float _DepthStrength;
			float _DepthThreshHold;
			float _DepthThickness;
			float4 _OutlineColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

			void BlendOverlay_float4(float4 Base, float4 BlendFactor, float Opacity, out float4 Out) {
				Out = 1.0 - abs(1. - -BlendFactor - Base);
				Out = lerp(Base, Out, Opacity);
			}

            float4 frag (v2f i) : SV_Target
            {
				float2 uv = i.uv;
				float toSaveTo;
				CustomSobel_float(uv, _Thickness, _DepthCustom, toSaveTo);
				toSaveTo = smoothstep(0, _DepthThreshHold, toSaveTo);
				toSaveTo = pow(toSaveTo, _DepthThickness);
				toSaveTo = mul(_DepthStrength, toSaveTo);

                // sample the texture
                float4 col = tex2D(_MainTex, i.uv);
				BlendOverlay_float4(col, _OutlineColor, toSaveTo, col);

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDHLSL
        }
    }
}
