﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAnimator : MonoBehaviour
{
    [SerializeField] AudioSource source;

   public void AudioSourcePlay()
   {        
     source.Play();      
   }

    public void AudioSourceStop()
    {              
      source.Stop();       
    }
}
