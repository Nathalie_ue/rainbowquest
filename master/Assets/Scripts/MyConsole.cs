﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyConsole : MonoBehaviour
{
    [SerializeField] GameObject console;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            if (console.activeInHierarchy)
            {
                Time.timeScale = 1;
            }
            else
            {
                Time.timeScale = 0;
            }
            console.SetActive(!console.activeInHierarchy);
        }
    }
}
