﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour2
{
    public delegate void extraEventsOnBlack();
    public extraEventsOnBlack eventHandler;

    [SerializeField]
    private Image FadeImage;
    private Color originalFadeColor;


    private float alphaValue, fadeSpeed;

    public bool fade;
    bool fadeIn = true, fadeOut, changeTime;

    private void Start()
    {
        originalFadeColor = FadeImage.color;
    }

    private void Update()
    {      
        if (fade)
        {
            
            if (fadeIn)         //fading in
            {
                alphaValue = SmoothLerpValue(alphaValue, 1, 0.05f, fadeSpeed);
                FadeImage.color = new Color(originalFadeColor.r, originalFadeColor.g, originalFadeColor.b, alphaValue);

                if(alphaValue == 1)
                {
                    if(eventHandler != null) eventHandler();
                    eventHandler = null;    //clearing after one execution

                    fadeOut = true;
                    fadeIn = false;
                }
            }
            else if (fadeOut)       //fading out
            {
                alphaValue = SmoothLerpValue(alphaValue, 0, 0.05f, fadeSpeed);
                FadeImage.color = new Color(originalFadeColor.r, originalFadeColor.g, originalFadeColor.b, alphaValue);

                if (alphaValue == 0)
                {
                    fadeIn = true;
                    fadeOut = false;
                    fade = false; //ending fade
                }
            }          
        }

        if (changeTime)
        {
            if (fade)
            {
                if (Time.timeScale != 0.1f)
                {
                    SetTime(0.1f);
                }
            }
            else
            {
                if (Time.timeScale != 1)
                {
                    SetTime(1);
                    changeTime = false;
                }
            }
        }
    }

    public void FadeScreen(float fadeSpeed, bool influenceTime)     //activating the fadeChange, fadeSpeed should be between 0.1 and 1
    {
        if(fadeSpeed >= 1 || fadeSpeed <= 0)
        {
            Debug.Log("Die Nummer ist mir zu heftig :C der fadespeed sollte zwischen 0 und 1 sein, momentan ist er: " + fadeSpeed);
        }

        this.fadeSpeed = fadeSpeed;
        this.fadeSpeed = Mathf.Clamp(this.fadeSpeed, 0, 1);
        fade = true;

        changeTime = influenceTime;
    }
}
