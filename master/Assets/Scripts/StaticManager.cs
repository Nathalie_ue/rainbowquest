﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;       //for Regex
using System;       //for convert
using System.Text;      //for string builder
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public static class StaticManager 
{
    private static Color originalTextColor;
    private static RectTransform paperPlaneTextTransform;
    public static TextMeshProUGUI paperPlaneText, dayFinishText;
    private static int xPos_UIString, yPos_UIString, xPos_FinishString;           //the "x" and "y" positions for the string above


    public static int paperPlaneCount = 0;
    public static List<SearchThroughAble> searchables = new List<SearchThroughAble>();
    public static List<GameObject> currentPaperPlanes = new List<GameObject>();

    private static GameObject player;
    private static GameObject mainCamera;
    public static CharacterController playerController;

    public static ScreenFader screenFade;

    private static string originalFinishString;
   
    public static void SetValues()     
    {
        Scene cachedScene = SceneManager.GetActiveScene();
        if(cachedScene.name != "Last Night")
        {
          dayFinishText = GameObject.Find("DayFinishText").GetComponent<TextMeshProUGUI>();
        }
        originalTextColor = paperPlaneText.color;
        player = GameObject.Find("Player");
        playerController = player.GetComponent<CharacterController>();
        mainCamera = Camera.main.gameObject;
        screenFade = GameObject.FindObjectOfType<ScreenFader>();

        if (cachedScene.name != "Last Night")
        {
            xPos_UIString = paperPlaneText.text.IndexOf("X");
            yPos_UIString = paperPlaneText.text.IndexOf("Y");
            xPos_FinishString = dayFinishText.text.IndexOf("X");
            originalFinishString = dayFinishText.text;
        }
        paperPlaneTextTransform = paperPlaneText.gameObject.GetComponent<RectTransform>();


        if (cachedScene.name != "Last Night") ChangeCount();
    }


    public static void ChangePlayerPosition(Vector3 targetPosition)
    {       
        mainCamera.transform.position = targetPosition + (mainCamera.transform.position - player.transform.position);

        playerController.enabled = false;
        player.transform.position = targetPosition;
        playerController.enabled = true;

    }

    private static Vector3 targetPositionVar;

    public static void ChangePlayerPositionAndFadeScreen(Vector3 targetPosition, float speed)
    {
        screenFade.FadeScreen(speed, true);
        targetPositionVar = targetPosition;
        screenFade.eventHandler += ChangePositionsAfterScreenFade;
    }

    public static void Test()
    {
        playerController.enabled = false;
    }

    private static void ChangePositionsAfterScreenFade()
    {
        mainCamera.transform.position = targetPositionVar + (mainCamera.transform.position - player.transform.position);

        playerController.enabled = false;
        player.transform.position = targetPositionVar;
        playerController.enabled = true;
    }

    private static string replace, pattern;     //for Regex stuff
    public static void ChangeCount()            //String change.
    {
        StringBuilder cachedString = new StringBuilder(paperPlaneText.text, 30);        //we need a stringbuilder to change char at pos

        cachedString[xPos_UIString] = Convert.ToChar(currentPaperPlanes.Count.ToString());         //putting in the number as a char
        cachedString[yPos_UIString] = Convert.ToChar(paperPlaneCount.ToString());         //putting in the number as a char       


        StringBuilder cachedString_Finish = new StringBuilder(originalFinishString, 30);

        if(currentPaperPlanes.Count == paperPlaneCount)
        {
            cachedString_Finish = new StringBuilder("Drücke" + "<color=#02BA0F>" + " 'E' " + "</color>" + "um die Nacht zu beenden");
            paperPlaneText.color = Color.green;
            paperPlaneTextTransform.localScale *= 0.15f;
            dayFinishText.text = cachedString_Finish.ToString();
        }
        else
        {
            paperPlaneTextTransform.localScale *= 0.3f;
            cachedString_Finish[xPos_FinishString] = Convert.ToChar((paperPlaneCount - currentPaperPlanes.Count).ToString());

            if (paperPlaneCount - currentPaperPlanes.Count != 1)
            {
                if (cachedString_Finish.ToString().Contains("Briefe"))
                {
                    pattern = @"Briefe";
                }
                else
                {
                    pattern = @"Brief";
                }
               
                replace = "Briefe";
            }
            else
            {
                //Debug.Log("lmao");            //archived lmao.

                if (cachedString_Finish.ToString().Contains("Briefe"))
                {
                    pattern = @"Briefe";
                }
                else
                {
                    pattern = @"Brief";
                }
               
                replace = "Brief";
            }

            string localCachedString = cachedString_Finish.ToString();
            localCachedString = Regex.Replace(localCachedString, pattern, replace);

            paperPlaneText.color = originalTextColor;
            dayFinishText.text = localCachedString;
        }     

        paperPlaneText.text = cachedString.ToString();
    }
    
    public static void ResetSearchables()
    {
        for (int i = 0; i < searchables.Count; i++)
        {
            if (searchables[i].hasPaperPlanePublic)
            {
                searchables[i].AddPaperPlane();
            }
        }
    }
}
