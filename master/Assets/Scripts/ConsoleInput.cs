﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConsoleInput : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    string compareString;

    private void Update()
    {
        text.text += Input.inputString;
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            text.text = "";
        }
        else if (Input.GetKeyDown(KeyCode.F6))
        {
            compareString = text.text;
            ExecuteMethods(text.text);           
        }
    }

    void ExecuteMethods(string input)
    {
        Debug.Log(input);

        switch (input)
        {
            case "win":               
                Debug.Log("you ended the day");
                ServiceLocator.finish.EndDay();
                break;

            case "lose":
                Debug.Log("you died");
                ServiceLocator.obstacleManager.Die();
                break;

            case "letter":
                Debug.Log("You gave yourself a cool letter");
                StaticManager.currentPaperPlanes.Add(gameObject);
                StaticManager.ChangeCount();
                break;
        }

        text.text = "";
    }
}
