﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour2
{
    [SerializeField] Transform toFollow;

    private float customZOffset;
    [SerializeField] private Vector3 offset;
    private Vector3 followVector;

    [SerializeField, Range(0, 1)] private float smoothness;

    private void Start()
    {
        followVector = new Vector3(toFollow.position.x, toFollow.position.y, transform.position.z);
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(toFollow.position.x, toFollow.position.y, transform.position.z) + offset, smoothness * TimeMultiplication());
    }
}
