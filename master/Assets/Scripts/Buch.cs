﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buch : MonoBehaviour2           
{
    [SerializeField] private AudioClip[] collisionClips;
    [SerializeField] private AudioSource source;
    private bool once;

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player"))
        {
            if (!once)
            {
                MyPlay(source, collisionClips[Random.Range(0, collisionClips.Length - 1)]);
                ServiceLocator.obstacleManager.ChangeHealthValue(7);
                StartCoroutine(EnableObstacle());
                once = true;
            }
           
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            MyPlay(source, collisionClips[Random.Range(0, collisionClips.Length - 1)]);
        }
    }

    private IEnumerator EnableObstacle()
    {
        yield return new WaitForSecondsRealtime(2.5f);
        gameObject.layer = 0;
        GetComponent<Collider>().isTrigger = true;
        GetComponent<Rigidbody>().isKinematic = true;
    }
}
