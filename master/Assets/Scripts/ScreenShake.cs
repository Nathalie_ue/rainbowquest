﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour2
{
    Camera cam;
    [SerializeField] Transform follow;
    public float addMagnitude, addDuration;
    bool changeBackFOV;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    public IEnumerator Shake(float duration, float magnitude)
    {

        Vector3 originalPos = transform.localPosition;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-3f, 3f) * magnitude;

            float y = Random.Range(-3f, 3) * magnitude;


            Vector3 toAdd = transform.right * x + transform.up * y;
            transform.localPosition += toAdd * TimeMultiplication();

            elapsed += Time.deltaTime;

            yield return null;

        }

    }

    private void Update()
    {
        if (changeBackFOV)
        {
            if(cam.fieldOfView != ServiceLocator.originalFOV)
            {
                cam.fieldOfView = SmoothLerpValue(cam.fieldOfView, ServiceLocator.originalFOV, 0.1f, 0.2f);
            }
            else
            {
                changeBackFOV = false;
            }
        }
    }

    public void ChangeBackToOriginalFOV()
    {
        changeBackFOV = true;
    }
}
