﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KI_Movement : MonoBehaviour2
{
    [SerializeField] private Vector2 minMaxWaitTime = new Vector2(2, 5);
    [SerializeField, Range(0, 0.5f)] float speed = 0.01f; 
    float threshhold = 0.25f;
    [SerializeField] GameObject toMove;
    [SerializeField] Transform[] points;
    [SerializeField] bool afterReachingLastPointsGobackToBeginning, usePosition = true, useRotation = false;
    int currentIndex;

    float timer;

    bool dir;

    [SerializeField] Vector3[] manualRotation; //because of unity's awful quaternion integration, or my awful understanding of quaternions, this could be useful
    Vector3 toRotateTo, rotation;

    [SerializeField] AudioClip[] woodenSteps, carpetSteps;
    [SerializeField] AudioSource source;

    private void Start()
    {
        if(source != null)
        {
            if (woodenSteps.Length == 0)
            {
                woodenSteps = ServiceLocator.moveSounds.woodenSteps;
            }

            if (carpetSteps.Length == 0)
            {
                carpetSteps = ServiceLocator.moveSounds.carpetSteps;
            }
        }
       

        SetRotationVectors();
    }

    void SetRotationVectors()
    {
        if(manualRotation.Length != 0)
        {
            toRotateTo = manualRotation[currentIndex];
        }
        else
        {
            toRotateTo = points[currentIndex].transform.localEulerAngles;
        }       
        rotation = toMove.transform.localEulerAngles;
    }

    private bool Condition()
    {
        bool cachedBool;
        if (usePosition)
        {
            cachedBool = Vector3.Distance(toMove.transform.position, points[currentIndex].position) > threshhold;
        }
        else
        {
            cachedBool = Vector3.Distance(toMove.transform.localEulerAngles, points[currentIndex].localEulerAngles) > threshhold;
        }

        return cachedBool;
    }

    private void Update()
    {
        if(timer <= 0)
        {
            if (Condition())
            {
                if (usePosition)
                {
                    PlaySounds();
                    toMove.transform.position = Vector3.Lerp(toMove.transform.position, points[currentIndex].position, speed * TimeMultiplication());
                }
                if (useRotation)
                {
                    rotation = Vector3.Lerp(rotation, toRotateTo, speed * TimeMultiplication());
                    toMove.transform.localRotation = Quaternion.Euler(rotation);
                    
                }
            }
            else
            {
                timer = Random.Range(minMaxWaitTime.x, minMaxWaitTime.y);
                if (usePosition) toMove.transform.position = points[currentIndex].position;
                if (useRotation) toMove.transform.rotation = points[currentIndex].rotation;

                if (afterReachingLastPointsGobackToBeginning)
                {
                    if (currentIndex < points.Length - 1)
                    {
                        currentIndex++;
                    }
                    else
                    {
                        currentIndex = 0;
                    }
                }
                else
                {
                    
                    if (!dir)
                    {
                        if(currentIndex < points.Length - 1)
                        {
                            currentIndex++;
                        }
                        else
                        {
                            currentIndex--;
                            dir = true;
                        }
                    }
                    else
                    {

                        if (currentIndex > 0)
                        {
                            currentIndex--;
                        }
                        else
                        {
                            currentIndex++;
                            dir = false;
                        }
                    }
                }
               
                SetRotationVectors();
            }
        }
        else
        {
            timer -= Time.deltaTime;
        }

    }


    bool wasPlaying;
    [SerializeField, Range(0, 1)] float quantitave_frequency = 0.1f;
    float soundTimer;

    void PlaySounds()
    {
            if (soundTimer > 0)
            {
                soundTimer -= Time.deltaTime;
            }
            else
            {               
                PlaySound();
                soundTimer = quantitave_frequency;
            }       
    }

    void PlaySound()
    {
        //Debug.Log(currentMaterial);
        switch (currentMaterial)
        {
            case "Untagged":             
                MyPlay(source, woodenSteps[Random.Range(0, woodenSteps.Length)]);
                break;

            case "Carpet":
                MyPlay(source, carpetSteps[Random.Range(0, carpetSteps.Length)]);
                break;

            case "Ground":
                MyPlay(source, woodenSteps[Random.Range(0, woodenSteps.Length)]);
                break;
        }
    }

    private void FixedUpdate()
    {
        RayCheck();
    }

    [SerializeField] private LayerMask lay;
    private string currentMaterial = "Untagged";
    private RaycastHit hit;

    void RayCheck()
    {
        Physics.Raycast(toMove.transform.position, Vector3.up * -1, out hit, 4, lay);
        if (hit.collider != null)
        {
            if (currentMaterial != hit.collider.tag)
            {
                currentMaterial = hit.collider.tag;
            }
        }       
    }

}
