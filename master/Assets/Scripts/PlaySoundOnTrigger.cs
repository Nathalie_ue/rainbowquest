﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnTrigger : MonoBehaviour2
{
    [SerializeField] private float addPitch;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioClip[] startClip, endClip;
    [SerializeField] private bool useEnd = true;
    private bool endTicket;
    [SerializeField] private float addVolume;
    private float originalVolume;
    bool once;
    bool canPlay = true;

    private void Start()
    {
       
    }

    private void Update()
    {
        if (!once)
        {
            originalVolume = source.volume;
            once = true;
        }
        if (endTicket)
        {
            if (!source.isPlaying)
            {
                MyPlay(source, endClip[Random.Range(0, endClip.Length - 1)]);
                endTicket = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && canPlay)
        {
            Debug.Log(other.gameObject.name);
            MyPlay(source, startClip[Random.Range(0, startClip.Length - 1)]);
            source.pitch += addPitch;
            source.volume = originalVolume + addVolume;
            StartCoroutine(PlayChangeStatus());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (useEnd)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                endTicket = true;
            }
        }     
    }

    private IEnumerator PlayChangeStatus()
    {
        canPlay = false;
        yield return new WaitForSeconds(0.1f);
        canPlay = true;
    }
}
