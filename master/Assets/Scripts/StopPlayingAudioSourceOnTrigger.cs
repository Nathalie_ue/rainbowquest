﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopPlayingAudioSourceOnTrigger : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    private void OnTriggerEnter(Collider other)
    {
        source.Stop();
    }
}
