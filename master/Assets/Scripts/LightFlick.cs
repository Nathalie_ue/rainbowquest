﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlick : MonoBehaviour
{
    Light light;
    float inten;
    public float intensity, start;

    [Range(0, 1)]
    public float lerp;

    void Start()
    {
        light = GetComponent<Light>();
    }


    void Update()
    {
        inten = Random.Range(start - intensity, start + intensity);

        light.intensity = Mathf.Lerp(light.intensity, inten, lerp);
        light.range = Mathf.Lerp(light.intensity, inten, lerp);

    }
}
