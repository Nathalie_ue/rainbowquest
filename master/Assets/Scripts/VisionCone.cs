﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionCone : MonoBehaviour
{
    [SerializeField]
    string trigger;

    [SerializeField]
    Animator parentalAnimator;

    private void OnTriggerEnter(Collider other)
    {
        parentalAnimator.SetTrigger(trigger);
    }
}
