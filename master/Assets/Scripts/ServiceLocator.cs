﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;

public static class ServiceLocator
{
    public static float originalFOV, originalVignetteValue;

    public static AdvancedCameraController advCamController;
    public static Transform Player;
    public static GameObject mainCamera;
    public static Camera mainCamCam;
    public static ScreenShake shake;
    public static Obstacles obstacleManager;
    public static MyAudioManager myAudioManager;
    public static FinishDay finish;
    public static PlayerMovementSounds moveSounds;


    //PP stuff

    public static Vignette vignette;

    public static PostProcessVolume volume;




    public static void SetValues()
    {
        obstacleManager = GameObject.FindObjectOfType<Obstacles>();
        finish = GameObject.FindObjectOfType<FinishDay>();
        myAudioManager = GameObject.FindObjectOfType<MyAudioManager>();
        volume = GameObject.FindObjectOfType<PostProcessVolume>();
        volume.profile.TryGetSettings<Vignette>(out vignette);
        Player = GameObject.Find("Player").transform;
        shake = GameObject.FindObjectOfType<ScreenShake>();
        advCamController = GameObject.FindObjectOfType<AdvancedCameraController>();
        mainCamera = Camera.main.gameObject;
        mainCamCam = mainCamera.GetComponent<Camera>();
        originalFOV = mainCamCam.fieldOfView;
        originalVignetteValue = vignette.intensity.value;
        moveSounds = GameObject.FindObjectOfType<PlayerMovementSounds>();
    }


    public static void ResetVignette()
    {
        vignette.intensity.value = originalVignetteValue;
    }
}
