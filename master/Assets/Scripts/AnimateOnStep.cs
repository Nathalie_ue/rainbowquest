﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateOnStep : MonoBehaviour2
{
    [SerializeField] private AudioClip input, output;
    [SerializeField] private AudioSource audiosource;
    [SerializeField] private string boolToSet;
    private bool canPlayAudio;
    private bool playOnce;
    [SerializeField] private Animator chosenAnimator;

    private void Start()
    {
        if(audiosource != null)
        {
            canPlayAudio = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            Debug.Log(other.gameObject.name);
        {
            if (canPlayAudio && !playOnce)
            {
                MyPlay(audiosource, input);
                playOnce = true;
            }
            chosenAnimator.SetBool(boolToSet, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (canPlayAudio && playOnce)
            {
                MyPlay(audiosource, output);
                playOnce = false;
            }
            chosenAnimator.SetBool(boolToSet, false);
        }
    }
}
