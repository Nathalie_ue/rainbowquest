﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Obstacles : MonoBehaviour2
{
    [SerializeField] private AudioClip loseSound;
    private AudioSource loseSource;

    [SerializeField]
    private string obstacleTag = "Obstacle";
    private string letterTag = "Letter";
    
    [SerializeField]
    private Image healthBar;

    [SerializeField]
    private float health = 100;
    private float damage = 1;
    private float maxHealth = 100;
 
    [SerializeField]
    private Transform teleportTarget;

    public CharacterController playerController;

    [SerializeField]
    private GameObject[] letters;


    [SerializeField] TextMeshProUGUI loseText;
    private Color originalColor, noOpacityColor;
    private bool textFadeDir;

    private RectTransform healthBarParent;              //not using exposed variable, because every level would need change in inspector
    private Vector3 originalHealthScale, setHealthScale;
    private bool lerpScaleBack;


    private void Start()
    {
        loseSource = GetComponent<AudioSource>();
        if (!loseText) loseText = GameObject.Find("LoseText").GetComponent<TextMeshProUGUI>();
        originalColor = loseText.color;
        noOpacityColor = new Color(originalColor.r, originalColor.g, originalColor.b, 0);
        loseText.color = noOpacityColor;

        letters = GameObject.FindGameObjectsWithTag(letterTag);
        playerController = GetComponent<CharacterController>();
        teleportTarget.position = transform.position;           //as long as this script stays at the player GO this will work

        health = maxHealth;
        healthBar.fillAmount = 0;

        healthBarParent = healthBar.transform.parent.GetComponent<RectTransform>();
        originalHealthScale = healthBarParent.localScale;
        setHealthScale = new Vector3(0.001f, 0.001f, 0.001f);
        healthBarParent.localScale = setHealthScale;
    }

    private void OnTriggerStay(Collider other)
    {
        //Wenn der Spieler ein Hinderniss berührt, nimmt er Schaden
        if(other.gameObject.tag == obstacleTag)
        {
            additionalScale = 0.006f;
            health -= damage;
            healthBar.fillAmount = 1 - (health / maxHealth);
            ServiceLocator.vignette.intensity.value = ServiceLocator.originalVignetteValue + (0.2f * (1 - (float)health/100));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // ServiceLocator.mainCamCam.fieldOfView = Mathf.Lerp(ServiceLocator.mainCamCam.fieldOfView, ServiceLocator.originalFOV * 1.1f, 0.3f * Time.deltaTime * 100);
        if (other.gameObject.tag == obstacleTag)
        {
            lerpScaleBack = false;
           // healthBarParent.localScale = originalHealthScale;
            //StartCoroutine(ServiceLocator.shake.Shake(0.005f, 0.3f)); 
        }      

    }

    public void ChangeHealthValue(int damageLocal)
    {
        additionalScale = 0.006f;
        StartCoroutine(ChangeScaleBool());
        health -= damage * damageLocal;      
        healthBar.fillAmount = 1 - (health / maxHealth);
        ServiceLocator.vignette.intensity.value = ServiceLocator.originalVignetteValue + (0.2f * (1 - (float)health / 100));
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == obstacleTag)
        {
           StartCoroutine(ChangeScaleBool());
          //  ServiceLocator.shake.ChangeBackToOriginalFOV();
        }
    }

    IEnumerator ChangeScaleBool()
    {
        yield return new WaitForSecondsRealtime(2f);
        lerpScaleBack = true;
    }

    private float additionalScale;

    private void Update()
    {
        healthBarParent.localScale = Vector3.Lerp(healthBarParent.localScale, originalHealthScale * (1 - (health / maxHealth)) + new Vector3(additionalScale, additionalScale, additionalScale), 0.1f * TimeMultiplication());

        if (health <= 0)
        {
            Die();
        }

        if (textFadeDir)
        {
            loseText.color = Color.Lerp(loseText.color, originalColor, 0.7f * TimeMultiplication());
        }
        else
        {
            if(loseText.color != noOpacityColor)
            {
                loseText.color = Color.Lerp(loseText.color, noOpacityColor, 0.7f * TimeMultiplication());
            }           
        }

        if (lerpScaleBack)
        {
            //health = SmoothLerpValue(health, maxHealth, 0.1f, 0.001f * TimeMultiplication());
            if (health < maxHealth) health += Time.deltaTime * 5;
            healthBar.fillAmount = 1 - (health / maxHealth);
            ServiceLocator.vignette.intensity.value = ServiceLocator.originalVignetteValue + (0.2f * (1 - (float)health / 100));
        }

        if(health >= maxHealth) additionalScale = Mathf.Lerp(additionalScale, 0, 0.1f * TimeMultiplication());
    }

    public void Die()
    {
        if (!loseSource.isPlaying)
        {
            MyPlay(loseSource, loseSound);
            loseSource.pitch /= Time.timeScale;
        }
        StaticManager.screenFade.FadeScreen(0.15f, true);
        StaticManager.screenFade.eventHandler += MyReset;
        StartCoroutine(FadeText(0.3f));
    }



    //Wenn der Spieler stirbt, wird er zurückgesetzt
    //und der Slider füllt sich wieder

    private void MyReset()
    {
        ResetPlayer();
        ResetLetter();
    }

    private void ResetPlayer()
    {
        StaticManager.ChangePlayerPosition(teleportTarget.position);
        ServiceLocator.ResetVignette();

        //Leben wird erst zurück gesetzt, wenn der Spieler zurückgesetzt wurde
        
        health = maxHealth;
        healthBar.fillAmount = 1 - (health / maxHealth);
    }

    private void ResetLetter()
    {
        foreach (GameObject letter in StaticManager.currentPaperPlanes)
        {
            letter.SetActive(true);
        }

        StaticManager.currentPaperPlanes.Clear();
        StaticManager.ResetSearchables();
        StaticManager.ChangeCount();        
    }

    private IEnumerator FadeText(float delay)
    {
        textFadeDir = true;
        yield return new WaitForSeconds(delay);
        textFadeDir = false;
    }
}
