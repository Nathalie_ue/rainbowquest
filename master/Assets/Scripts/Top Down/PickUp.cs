﻿using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.EventSystems;

public class PickUp : MonoBehaviour
{
    private string letterTag = "Letter";

    [SerializeField]
    private GameObject tutorialText;

    [SerializeField]
    private GameObject tutorialLetter;
    
    private PlayerMovement freezeRotation;

    public GameObject selectedButton;

    [SerializeField] private AudioClip[] letterClips;

    private void Start()
    {
        freezeRotation = GetComponent<PlayerMovement>();
    }

    Collider savedCollider;
    bool canPickup;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == letterTag || other.gameObject == tutorialLetter)
        {
            savedCollider = other;
            canPickup = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == letterTag || other.gameObject == tutorialLetter)
        {
            canPickup = false;
        }
    }

    private void Update()
    {
        if (canPickup)
        {
            PickUpStuff();
        }
    }

    void PickUpStuff()
    {
        if (Input.GetKeyDown(KeyCode.E) && savedCollider.gameObject.tag == letterTag)
        {
            if(StaticManager.currentPaperPlanes.Count == StaticManager.paperPlaneCount - 1)
            {
               ServiceLocator.myAudioManager.PlaySoundEffectLouder(letterClips[Random.Range(0, letterClips.Length)]);
            }
            else
            {
                ServiceLocator.myAudioManager.PlaySoundEffect(letterClips[Random.Range(0, letterClips.Length)]);
            }
            savedCollider.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.E) && savedCollider.gameObject == tutorialLetter)
        {
            Cursor.visible = true;
            tutorialText.SetActive(true);

            Time.timeScale = 0f;
            freezeRotation.useWobblyBackRotation = false;

            //clear the selected object of the event system
            EventSystem.current.SetSelectedGameObject(null);
            //Set a new selected object
            EventSystem.current.SetSelectedGameObject(selectedButton);

        }
    }

    public void CloseLetter()
    {
        Cursor.visible = false;
        tutorialText.SetActive(false);
        
        Time.timeScale = 1f;
        freezeRotation.useWobblyBackRotation = true;
    }    
}
