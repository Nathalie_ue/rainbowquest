﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    [SerializeField] Transform camera;
    void Update()
    {
        transform.forward = camera.forward;
    }
}
