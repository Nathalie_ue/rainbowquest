﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowing : MonoBehaviour
{
    [SerializeField] Transform toFollow;

    [SerializeField] private Vector3 offset;

    [SerializeField, Range(0, 0.5f)] private float smoothness = 0.1f;

    private void FixedUpdate()
    {
       if(Time.timeScale == 1) TopDownFollowing();
    }

    private void TopDownFollowing()
    {
        Vector3 positionVectorToFollow = new Vector3(toFollow.position.x, toFollow.position.y, toFollow.position.z);
        transform.position = Vector3.Lerp(transform.position, positionVectorToFollow + offset, smoothness * Time.deltaTime * 100);
    }
}
