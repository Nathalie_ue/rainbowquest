﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class FinishDay : MonoBehaviour
{
    [SerializeField]
    private string sceneToLoad;
    private bool CanGiveInput;

    private void Awake()
    {
        //ResetPaperPlaneValues();
    }

    private void Start()
    {
        Debug.Log(gameObject.name);
        //ServiceLocator.finish = this;
    }

    private void Update()
    {
        if (CanGiveInput)
        {
            if (StaticManager.paperPlaneCount == StaticManager.currentPaperPlanes.Count)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    EndDay();
                }
            }
        }
    }

    public void EndDay()
    {
        SceneManager.LoadScene(sceneToLoad);
        ResetPaperPlaneValues();
    }

    private void OnTriggerEnter(Collider other)     // "OnTriggerStay" is in my experience pretty inconsistent when used for input checks
    {                                               // thus I converted it to use update instead
        CanGiveInput = true;
    }

    private void OnTriggerExit(Collider other)
    {
        CanGiveInput = false;
    }

    private void ResetPaperPlaneValues()
    {
        StaticManager.paperPlaneCount = 0;          //reset of collectables for next level
        StaticManager.currentPaperPlanes.Clear();
        StaticManager.searchables.Clear();          //also resetting searchables
    }

    private void OnDisable()
    {
        ResetPaperPlaneValues();        //when new scene loads/main menu loads
    }


}
