﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour
{
    [SerializeField]
    private Transform teleportTarget;

    [SerializeField]
    private GameObject player;

    [SerializeField] AudioSource audioSource;
     
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StaticManager.ChangePlayerPositionAndFadeScreen(teleportTarget.position, 0.5f);
            if (audioSource != null) audioSource.Play();
        }       
    }
}
