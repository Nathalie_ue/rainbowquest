﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour2
{
    private delegate void effects();        //A delegate void to control the effects set
    effects effectsHandler;
    private CharacterController controller;

    [SerializeField] private float speed, maxGravity;
    private float internGravityValue;

    [SerializeField] private bool useRotationEffect;
    public bool useWobblyBackRotation;
    private bool grounded;
    CameronCollisionCheckWall wallCheck;

    PlayerMovementSounds movementSounds;

    //START METHODS

    private void Awake()
    {
        SetEffects();
        controller = GetComponent<CharacterController>();
    }

    private void Start()
    {
        wallCheck = GetComponentInChildren<CameronCollisionCheckWall>();
        movementSounds = GetComponentInChildren<PlayerMovementSounds>();
        internGravityValue = maxGravity;       
    }

    void SetEffects()       //effects to subscribe to the delegate
    {
        if (useRotationEffect) effectsHandler += RotateTowardsMoveDir;
    }


    //METHODS PER FRAME//

    private void Update()
    {
        if(effectsHandler != null)effectsHandler();
        PlayMovementParticle();
        movementSounds.canPlay = !wallCheck.isColliding;
    }

    private void FixedUpdate()
    {        
        CoreMovement();
    }

    // METHODS WHICH INVOLVE PLAYER INPUT//

    private float inputX, inputY, inputXRaw, inputYRaw;

    private void CoreMovement()     //The Velocity is set in here
    {
        inputXRaw = Input.GetAxisRaw("Horizontal");
        inputX = Input.GetAxis("Horizontal");

        inputYRaw = Input.GetAxisRaw("Vertical");
        inputY = Input.GetAxis("Vertical");

        GetLatestInput(new Vector2(inputXRaw, inputYRaw));
        CustomAcceleration();       //variable "internspeed", the main speed, resides here!       

        controller.Move(new Vector3(inputX, internGravityValue, inputY) * Time.deltaTime * internSpeed * (4.4f - (Mathf.Abs(inputX) + Mathf.Abs(inputY))));
    }

   [SerializeField] GameObject MC_MeshFront, MC_MeshBack, MC_MeshLeft, MC_MeshRight;

    void GetLatestInput(Vector2 inputVector)                //because there are no switch cases for vectors and I need precise control over input[And am tired] I had to write this atrocity
    {
        if(inputVector.y == 0 && inputVector.x != 0)
        {
            switch (inputVector.x)
            {
                case 1:
                    SetCameronRightMeshActive();
                    break;
                case -1:
                    SetCameronLeftMeshActive();
                    break;
            }
        }
        else if(inputVector.y != 0 && inputVector.x == 0)
        {
            switch (inputVector.y)
            {
                case 1:
                    SetCameronBackMeshActive();
                    break;
                case -1:
                    SetCameronFrontMeshActive();
                    break;
            }
        }
        else if(inputVector.magnitude == 0)
        {
            SetCameronFrontMeshActive();
        }
    }


   public void SetCameronFrontMeshActive()
   {
        if (!MC_MeshFront.activeInHierarchy)
        {
            MC_MeshFront.SetActive(true);
            MC_MeshBack.SetActive(false);
            MC_MeshRight.SetActive(false);
            MC_MeshLeft.SetActive(false);
        }
   }

    public void SetCameronBackMeshActive()
    {
        if (!MC_MeshBack.activeInHierarchy)
        {
            MC_MeshFront.SetActive(false);
            MC_MeshBack.SetActive(true);
            MC_MeshRight.SetActive(false);
            MC_MeshLeft.SetActive(false);
        }
    }

    public void SetCameronRightMeshActive()
    {
        if (!MC_MeshRight.activeInHierarchy)
        {
            MC_MeshFront.SetActive(false);
            MC_MeshBack.SetActive(false);
            MC_MeshRight.SetActive(true);
            MC_MeshLeft.SetActive(false);
        }

    }

    public void SetCameronLeftMeshActive()
    {
        if (!MC_MeshLeft.activeInHierarchy)
        {
            MC_MeshFront.SetActive(false);
            MC_MeshBack.SetActive(false);
            MC_MeshRight.SetActive(false);
            MC_MeshLeft.SetActive(true);
        }
         
    }



    [SerializeField, Range(0, 1)] float accelerationSpeed = 0.1f, deccelerationSpeed = 0.05f;
    [HideInInspector]public float internSprint;
    private float sprintMultiplier = 0.5f, internSpeed;

    private void CustomAcceleration()         //better controlled Acceleration. Acceleration achieved through custom inherited method
    {
        if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftAlt))
        {
            internSprint = SmoothLerpValue(internSprint, sprintMultiplier, 0.1f, 0.8f); ;
        }
        else if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftAlt))
        {
            internSprint = SmoothLerpValue(internSprint, 1, 0.1f, 0.8f);
        }

        if (inputXRaw != 0 || inputYRaw != 0)
        {
            if (internSpeed != speed * internSprint)
            {
                internSpeed = SmoothLerpValue(internSpeed, speed * internSprint, 0.1f, accelerationSpeed * Time.deltaTime * 100);                        
            }
        }
       else
       {
            if(internSpeed != 0)
            {
                internSpeed = SmoothLerpValue(internSpeed, 0, 0.1f, deccelerationSpeed * Time.deltaTime * 100);
            }
       }       
    }

    //EFFECTS//

    [SerializeField, Range(0, 1)] private float wobbleIntensity = 0.5f;
    Vector3 rotateBack;
    Vector3 rotationVector, backRotationVector;

    private void RotateTowardsMoveDir()
    {
        if (useWobblyBackRotation)
        {
            if ((inputXRaw != 0 || inputYRaw != 0) && !wallCheck.isColliding)
            {
                rotationVector = Vector3.Lerp(rotationVector, new Vector3(6 * inputYRaw * internSprint, transform.rotation.eulerAngles.y, -6 * inputXRaw * internSprint), 0.2f * Time.deltaTime * 100 * Time.timeScale);
            }
            else if ((inputXRaw == 0 && inputYRaw == 0) || wallCheck.isColliding)
            {
                if(Time.timeScale != 1)
                {
                    backRotationVector = Vector3.Lerp(backRotationVector, (Vector3.zero - rotationVector) * wobbleIntensity, 0.95f * Time.deltaTime * 100);

                    rotationVector += backRotationVector;
                }
                else
                {
                    backRotationVector = Vector3.Lerp(backRotationVector, (Vector3.zero - rotationVector) * wobbleIntensity, 0.1f * Time.deltaTime * 100);

                    rotationVector += backRotationVector;
                }
            }
        }
        else
        {
            rotationVector = Vector3.Lerp(rotationVector, new Vector3(6 * inputYRaw * internSprint, transform.rotation.eulerAngles.y, -6 * inputXRaw * internSprint), 0.2f * Time.deltaTime * 100);
        }            

        transform.rotation = Quaternion.Euler(rotationVector);      

    }


    [SerializeField] ParticleSystem movementParticleSystem, stoppingParticleSystem;
    private void PlayMovementParticle()
    {
        if ((inputXRaw != 0 || inputYRaw != 0) && !wallCheck.isColliding)
        {
            if(!movementParticleSystem.isPlaying) movementParticleSystem.Play();
        }
        else if ((inputXRaw == 0 && inputYRaw == 0) || wallCheck.isColliding)
        {
            if (movementParticleSystem.isPlaying)
            {
                StartCoroutine(PlayNonRepeatingParticleSystem(stoppingParticleSystem, 1));
                movementParticleSystem.Stop();
            }
        }
    }


    private IEnumerator PlayNonRepeatingParticleSystem(ParticleSystem toPlay, float delay)
    {
        toPlay.Play();
        yield return new WaitForSeconds(delay);
        toPlay.Stop();
    }

    //UNITY PROVIDED COLLISION CHECKS//
}
