﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpottedAnimation : MonoBehaviour2
{
    [SerializeField] private Transform scaleRef;
    Vector3 originalScale;

    private void Start()
    {
        if (scaleRef == null) scaleRef = transform;
        originalScale = scaleRef.localScale;
    }

    private void Update()
    {
        // scaleRef.localScale = Vector3.Lerp(scaleRef.localScale, originalScale, 0.02f * TimeMultiplication());
        //if(scaleRef.localScale != originalScale) scaleRef.localScale = WobbleLerp(scaleRef.localScale, originalScale, 0.7f);
        WobbleBackRot();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            float ranMul = Random.Range(1.3f, 2f);  //random multiplier
            Debug.Log("lebron");
            scaleRef.localScale = new Vector3(originalScale.x / ranMul, originalScale.y * ranMul, originalScale.z / ranMul);
        }
    }

    Vector3 x, scaleVec;
    Vector3 WobbleLerp(Vector3 from, Vector3 target, float speed)
    {
        x += Vector3.Lerp(x, (target - from) * 0.95f, speed * TimeMultiplication());
        return x;
    }

    void WobbleBackRot()
    {
        x = Vector3.Lerp(x, (originalScale - scaleRef.localScale), 0.2f * Time.deltaTime * 100);

        scaleVec += x;

        scaleRef.localScale = scaleVec;
    }
}
