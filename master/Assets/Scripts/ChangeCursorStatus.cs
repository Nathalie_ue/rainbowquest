﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCursorStatus : MonoBehaviour
{
    [SerializeField] private bool cursorVisibility;

    private void Start()
    {
        Cursor.visible = cursorVisibility;
    }
}
