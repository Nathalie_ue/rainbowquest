﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchThroughAble : MonoBehaviour2
{
    [SerializeField] GameObject ui_hint;
    [SerializeField] private float wobbleIntensity = 0.5f, wobbleSpeed = 1;
    private bool canSearchForItems, hasPaperPlane;
    [SerializeField] Transform spawnOutputPoint;
    [SerializeField] private GameObject output, emptyText;
    Vector3 originalScale, add;

    [HideInInspector] public bool hasPaperPlanePublic;
    bool spawnedPlane;
    PaperPlane possiblePlane;

    private void Awake()
    {
        this.enabled = false;
    }
    private void Start()
    {
        this.enabled = false;
        StaticManager.searchables.Add(this);
        ui_hint.SetActive(false);
        originalScale = transform.localScale;
      
        AddPaperPlane();
    }

    public void AddPaperPlane()
    {
        output.SetActive(false);
        possiblePlane = output.GetComponent<PaperPlane>();
        if (possiblePlane != null)
        {
            spawnedPlane = false;
            possiblePlane.searchParent = gameObject;
            possiblePlane.isUsedInSearch = true;
            possiblePlane.wasUsedInSearch = true;
            possiblePlane.gameObject.transform.position = spawnOutputPoint.position;
            possiblePlane.transform.rotation = Quaternion.Euler(possiblePlane.transform.eulerAngles.x, possiblePlane.transform.eulerAngles.y, -34.682f);     //for now hard coding rotation value    
            possiblePlane.body.constraints = RigidbodyConstraints.None;                         //also resetting rigidbody values
            possiblePlane.body.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
            possiblePlane.gameObject.SetActive(false);
            hasPaperPlane = true;
            hasPaperPlanePublic = true;
        }
        else
        {
            hasPaperPlanePublic = false;
            spawnedPlane = true;
        }
       
    }

    private float maxCoolOffTime = 0.5f;
    private float internTimer;

    private void Update()
    {
        if (internTimer <= 0)
        {
            if (canSearchForItems)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    PutOut();
                    internTimer = maxCoolOffTime;
                }
            }
        }
        else
        {
            internTimer -= Time.deltaTime;
        }
        

        if(transform.localScale != originalScale)
        {
            add = Vector3.Lerp(add, (originalScale - transform.localScale) * wobbleSpeed, 0.1f);
            Vector3 pass = transform.localScale + (add * TimeMultiplication());
            transform.localScale = pass;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ui_hint.SetActive(true);
            canSearchForItems = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ui_hint.SetActive(false);
            canSearchForItems = false;
        }
    }

    private void PutOut()
    {
        transform.localScale *= 1 - wobbleIntensity;
        if (!spawnedPlane)
        {
            if (!hasPaperPlane)
            {
                Instantiate(emptyText, spawnOutputPoint.position, Quaternion.identity);
            }
            else
            {
               // possiblePlane.body.AddForce(possiblePlane.transform.right * -6 * TimeMultiplication(), ForceMode.Impulse);
                possiblePlane.gameObject.SetActive(true);
                possiblePlane.gameObject.transform.parent = null;
                possiblePlane.isUsedInSearch = false;
                hasPaperPlane = false;
                // possiblePlane.gameObject.transform.parent = null;
            }
        }
        else
        {
            if(emptyText != null && output == null)
            {
                Instantiate(emptyText, spawnOutputPoint.position, Quaternion.identity);
            }
            else
            {
                Instantiate(output, spawnOutputPoint.position, Quaternion.identity);
                output.SetActive(true);
                output.transform.position = spawnOutputPoint.position;
                output = emptyText;
            }
        }
       
    }
}
