﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour2
{
    [SerializeField] AudioClip metallic;
    [SerializeField] AudioClip[] woodenClipsShort, woodenClipsLong;
    [SerializeField] AudioSource audioSource, metallicSourec;
    [SerializeField] private float clampAngles = 180;
    [SerializeField] private float weight = 2;
    [SerializeField] private Rigidbody rigidBody;
    [SerializeField] private float targetYAngle = 0;            //0 if horizontal, 90 if vertical etc.
    [SerializeField] private bool invertForces;
    private int multiplier;
    private Vector3 originalPos;
    float soundCoolOff;

    private void Start()
    {
        if (invertForces) multiplier = -1;      //for inverting forces
        else multiplier = 1;

        originalPos = transform.position;

        GetComponent<Collider>().isTrigger = true;      //I know it is pretty lazy to do it this way, but ill have to think for an alternative fix (if there is one available) and want to save some time
    }

    void FixedUpdate()               
    {
        transform.position = originalPos;       //for some goddamn unholy reason the world will collapse if i dont update the position every goddamn frame, because, why sould rigidbody constraints work, right? grr im angery and frustration      
        RotateToOrigin();       //yRotation is set here

        Vector3 rotationVector = new Vector3(0, yRotation, 0);
        transform.rotation = Quaternion.Euler(rotationVector);
    }

    private void Update()
    {
        if (soundCoolOff > 0) soundCoolOff -= Time.deltaTime;
    }


    private float yRotation;
    private float targetY;
    bool pause;

    void RotateToOrigin()
    {
        if (!pause)
        {
            if (!lerpTicket)
            {
                yRotation = SmoothLerpValue(yRotation, targetYAngle, 0.5f, 0.01f * TimeMultiplication());
            }
            else
            {
                if (Mathf.Abs(yRotation - targetY) > 3)
                {
                    yRotation = SmoothLerpValue(yRotation, targetY, 2, 0.01f * TimeMultiplication());

                    if (Mathf.Abs(targetYAngle - yRotation) + 10 > Mathf.Abs(clampAngles))  //buffer
                    {
                        PlayMySound(woodenClipsLong);
                        lerpTicket = false;
                    }
                }
                else
                {
                    PlayMySound(woodenClipsLong);
                    lerpTicket = false;
                }
            }
            yRotation = Mathf.Clamp(yRotation, targetYAngle - clampAngles, targetYAngle + clampAngles);
        }
      
    }


    private bool lerpTicket;

    private void AddToY(float toAdd)
    {
        targetY = (yRotation + toAdd) * multiplier;
        lerpTicket = true;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!lerpTicket && transform.localEulerAngles.y != targetYAngle)        //basically "if closing"
            {
                pause = true;
            }
            Debug.Log(transform.localEulerAngles.y + " and " + targetYAngle);
            PlayMetalSound();
            PlayMySound(woodenClipsShort);
            //First we check if the player is right/left from the transform through the sign of the dot of this right and distance vector between this and collision, which is afterwards multiplied with player velocity
            AddToY(Mathf.Sign(Vector3.Dot(transform.right, (collision.transform.position - transform.position))) * StaticManager.playerController.velocity.magnitude * (8 / weight) + 1);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        //Debug.Log(gameObject.name + collision.gameObject.name);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {            
            PlayMetalSound();
            PlayMySound(woodenClipsShort);
            //First we check if the player is right/left from the transform through the sign of the dot of this right and distance vector between this and collision, which is afterwards multiplied with player velocity
            AddToY(Mathf.Sign(Vector3.Dot(transform.right, (other.transform.position - transform.position))) * StaticManager.playerController.velocity.magnitude * (5 / weight) + 14);
        }
    }

    void PlayMySound(AudioClip[] c)
    {
        if(soundCoolOff <= 0) MyPlay(audioSource, c[RandomNumber(c.Length)]);
        soundCoolOff = 0.5f;
    }

    void PlayMetalSound()
    {
        if (soundCoolOff <= 0) MyPlay(metallicSourec, metallic);
    }
}
