﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameronCollisionCheckWall : MonoBehaviour
{
    private float softening = 0.5f;
    public bool isColliding;

    private void Update()
    {
        Vector2 inputVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        transform.localPosition = (new Vector3(inputVector.x, 0, inputVector.y) / softening);
    }

    private void FixedUpdate()
    {
        isColliding = false;
    }


    private void OnTriggerEnter(Collider other)
    {
       // isColliding = true;
    }

    private void OnTriggerStay(Collider other)
    {
        isColliding = true;
    }

    private void OnTriggerExit(Collider other)
    {
       // isColliding = false;
    }
}
