﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWobbleInterpolationScale : MonoBehaviour2
{
    [SerializeField] float wobbleIntensity = 0.5f;
    Vector3 originalScale, add;
    RectTransform thisTransform;

    private void Awake()
    {
        thisTransform = GetComponent<RectTransform>();
        originalScale = thisTransform.localScale;
    }

    private void Update()
    {
        if(thisTransform.localScale != originalScale)
        {
            add = Vector3.Lerp(add, (originalScale - thisTransform.localScale) * wobbleIntensity, 0.1f);
            Vector3 pass = transform.localScale + add * TimeMultiplication();
            transform.localScale = pass;
        }
    }
}
