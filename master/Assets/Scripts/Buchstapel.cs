﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buchstapel : MonoBehaviour
{
    [SerializeField] private float force = 1;
    [SerializeField] private Rigidbody[] bookBodies;
    private bool once;

    private void OnTriggerEnter(Collider other)
    {
        if (!once && other.CompareTag("Player"))
        {
            for (int i = 0; i < bookBodies.Length; i++)
            {
                bookBodies[i].isKinematic = false;
                bookBodies[i].AddForce(new Vector3(Random.Range(-force * 2, force * 2), Random.Range(-force, force), Random.Range(-force * 2, force * 2)), ForceMode.Impulse);
            }
            gameObject.tag = "Untagged";
            once = true;
        }
        
    }
}
