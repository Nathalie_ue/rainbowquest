﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class EndGame : MonoBehaviour
{
    public GameObject levelEndComic;

    [SerializeField] private Image firstPanel, secondPanel, thirdPanel, backgroundImage, fadeOutImage;
    bool lerpImage;
    public float lerpSpeed = 0.5f;
    Color nonTransparentColor, nonTransparentPanel;
    public Color lightBackground, transparentPanel;

    [SerializeField]
    private TextMeshProUGUI endText;

    public Vector3 endPanelPosition, panelOnScreenPosition, rightPanelPosition;

    private void Start()
    {     
        nonTransparentColor = backgroundImage.color;
        backgroundImage.color = new Color(nonTransparentColor.r, nonTransparentColor.g, nonTransparentColor.b, 0);

        nonTransparentPanel = firstPanel.color;
        firstPanel.color = transparentPanel;
        

        //Positionen bestimmen
        backgroundImage.transform.localPosition = panelOnScreenPosition;
        firstPanel.transform.localPosition = panelOnScreenPosition;
        secondPanel.transform.localPosition = rightPanelPosition;
        thirdPanel.transform.localPosition = rightPanelPosition;
    }

    bool endGameTicket;
    private void Update()
    {
        if (lerpImage)
        {     
            if (!endGameTicket)
            {
                StartCoroutine(EndGameCoroutine());
                endGameTicket = true;
            }
        }
    }

    IEnumerator EndGameCoroutine()
    {
        float startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //Erstes Panel & schwarzer Hintergrund wird eingeblendet
            backgroundImage.color = Color.Lerp(backgroundImage.color, nonTransparentColor, (Time.time - startTime) / lerpSpeed);
            firstPanel.color = Color.Lerp(transparentPanel, nonTransparentPanel, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        backgroundImage.color = nonTransparentColor;
        firstPanel.color = nonTransparentPanel;

        FindObjectOfType<PlayerMovement>().enabled = false;       //disable movement

        yield return new WaitForSecondsRealtime(2.8f);

        startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //erstes Panel geht zur Seite, zweiter Panel wird eingeblendet
            firstPanel.transform.localPosition = Vector3.Lerp(panelOnScreenPosition, endPanelPosition, (Time.time - startTime) / lerpSpeed);
            secondPanel.transform.localPosition = Vector3.Lerp(rightPanelPosition, panelOnScreenPosition, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        firstPanel.transform.localPosition = endPanelPosition;
        secondPanel.transform.localPosition = panelOnScreenPosition;

        yield return new WaitForSecondsRealtime(2.8f);
                
        nonTransparentPanel = secondPanel.color;

        startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //zweiter Panel geht zur Seite
            //secondPanel.transform.localPosition = Vector3.Lerp(panelOnScreenPosition, endPanelPosition, (Time.time - startTime) / lerpSpeed);
            secondPanel.color = Color.Lerp(nonTransparentPanel, transparentPanel, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        secondPanel.transform.localPosition = endPanelPosition;
        secondPanel.color = transparentPanel;

        yield return new WaitForSecondsRealtime(1f);

        //letzter Panel wird transparent
        nonTransparentPanel = thirdPanel.color;
        thirdPanel.color = transparentPanel;
        thirdPanel.transform.localPosition = panelOnScreenPosition;

        startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //Hintergrund wird hell
            backgroundImage.color = Color.Lerp(nonTransparentColor, lightBackground, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        backgroundImage.color = lightBackground;

        startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //drittes Panel wird eingeblendet
            thirdPanel.color = Color.Lerp(transparentPanel, nonTransparentPanel, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        thirdPanel.color = nonTransparentPanel;

        nonTransparentPanel = endText.color;
        endText.color = transparentPanel;
        endText.transform.localPosition = panelOnScreenPosition;

        startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //Text wird eingeblendet
            endText.color = Color.Lerp(transparentPanel, nonTransparentPanel, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        endText.color = nonTransparentPanel;        

        yield return new WaitForSecondsRealtime(5f);

        nonTransparentPanel = fadeOutImage.color;
        fadeOutImage.color = transparentPanel;
        fadeOutImage.transform.localPosition = panelOnScreenPosition;

        startTime = Time.time;
        while (Time.time < startTime + lerpSpeed)
        {
            //FadeOut
            fadeOutImage.color = Color.Lerp(transparentPanel, nonTransparentColor, (Time.time - startTime) / lerpSpeed);

            yield return null;
        }
        fadeOutImage.color = nonTransparentColor;

        yield return new WaitForSecondsRealtime(0.5f);

        SceneManager.LoadScene("CreditsScene", LoadSceneMode.Single);               //ending the Game
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            FindObjectOfType<PlayerMovementSounds>().enabled = false;       //muting Player-steps
            levelEndComic.SetActive(true);
            lerpImage = true;   
        }
    }
}
