﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kickable : MonoBehaviour2
{
    [SerializeField] float weight = 2;
    [SerializeField] Rigidbody rigidBody;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            rigidBody.AddForce(StaticManager.playerController.velocity * (4 / weight), ForceMode.Impulse);
        }
        else if(other.gameObject.layer == 16) //if door
        {
            rigidBody.velocity = Vector3.zero;
            rigidBody.AddForce((transform.position - other.gameObject.transform.position), ForceMode.Impulse); //bounce off!
        }

    }  
   
}
