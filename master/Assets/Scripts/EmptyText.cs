﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyText : MonoBehaviour2
{
    // Start is called before the first frame update
    void Start()
    {
       // transform.LookAt(ServiceLocator.mainCamera.transform.position, -Vector3.up);
        Destroy(gameObject, 10);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * 0.1f * TimeMultiplication());
    }
}
