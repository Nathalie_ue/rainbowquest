﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderSeeThroughSync : MonoBehaviour
{
    [SerializeField, Range(0.5f, 5)] private float maxSize = 1;
    [SerializeField] private Material wallMaterial;
    [SerializeField] private Camera cam;

    [SerializeField] LayerMask layermask;

    [SerializeField] static int posID = Shader.PropertyToID("_Position");
    [SerializeField] static int sizeID = Shader.PropertyToID("_Size");
    [SerializeField] static int noiseID = Shader.PropertyToID("_Offset");

    private float originalNoiseScale, originalSize;
    private Vector3 originalPosition;

    void Start()
    {
        if (!cam) cam = Camera.main; 
        originalNoiseScale = wallMaterial.GetFloat(noiseID);
        originalSize = wallMaterial.GetFloat(sizeID);
        originalPosition = wallMaterial.GetVector(posID);
    }



    private Vector3 view, direction;
    private Ray ray;

    [SerializeField, Range(0, 1)] float lerpInOutSpeed = 0.1f;
    float size;
    RaycastHit hit;
    void Update()
    {
        direction = cam.transform.position - transform.position;
        ray = new Ray(transform.position, direction.normalized);


        if(Physics.Raycast(ray, Vector3.Distance(cam.transform.position, transform.position), layermask))
        {
            //Debug.Log(hit.collider);
            if(size < maxSize - (0.05f * maxSize))
            {
                size = Mathf.Lerp(size, maxSize, lerpInOutSpeed * Time.deltaTime * 100);
            }
            else
            {
                if (size != maxSize) size = maxSize;
            }
        }
        else
        {
            if (size > 0.05f)
            {
                size = Mathf.Lerp(size, 0, lerpInOutSpeed * Time.deltaTime * 100);
            }
            else
            {
                if (size != 0) size = 0;
            }
        }

        wallMaterial.SetFloat(sizeID, size);

        view = cam.WorldToViewportPoint(transform.position);
        wallMaterial.SetVector(posID, view);

        if(scrollSpeed != 0)
        {
            NoiseScroll();
        }
    }

    [SerializeField, Range(0, 5)] private float scrollSpeed;
    float internOffset;


    void NoiseScroll()
    {
        internOffset += Time.deltaTime * scrollSpeed;
        if (internOffset >= 20000) internOffset = 0;

        wallMaterial.SetFloat(noiseID, internOffset);
    }

    private void OnApplicationQuit()
    {
        ResetValues();
    }

    void ResetValues()
    {
        wallMaterial.SetFloat(noiseID, originalNoiseScale);
        wallMaterial.SetVector(posID, originalPosition);
        wallMaterial.SetFloat(sizeID, originalSize);
    }

    private void OnDrawGizmosSelected()
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawRay(transform.position, direction.normalized * 3000);
    }
}
