﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Initializer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI paperPlaneText;

    private void Awake()
    {
        if (Time.timeScale != 1) Time.timeScale = 1;            //resetting timescale if returned from pause/main menu
        StaticManager.paperPlaneText = paperPlaneText;      //definetly needs to happen first     

        ServiceLocator.SetValues();
       // StaticManager.SetValues();
    }

    void Start()            //In Start um alles korrekt zu setzen, da start erst am "Ende" Der initialization ausgeführt wird
    {
        StaticManager.currentPaperPlanes.Clear();       //making the clearance shure
       // ServiceLocator.SetValues();
        StaticManager.SetValues();
    }
}
