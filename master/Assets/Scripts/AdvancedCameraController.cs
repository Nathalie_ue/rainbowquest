﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedCameraController : MonoBehaviour2
{
    Vector3 originalRotation;

    [SerializeField] private float maxDegrees = 10;
    public float sensitivity = 100;

    private float rotationX, rotationY;
    private float mouseX, mouseY;

    void Start()
    {
        originalRotation = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        SetRotation();
    }


   

    void SetRotation()
    {            
            mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
            mouseY = Input.GetAxis("Mouse Y") * sensitivity* Time.deltaTime;

            rotationX += mouseX;
            rotationX = Mathf.Clamp(rotationX, -maxDegrees, maxDegrees);

            rotationY += mouseY;
            rotationY = Mathf.Clamp(rotationY, -maxDegrees , maxDegrees );

            Vector3 myRotation = originalRotation + new Vector3(-rotationY, rotationX, 0);

            transform.rotation = Quaternion.Lerp(Quaternion.Euler(transform.localEulerAngles), Quaternion.Euler(myRotation), 0.02f * TimeMultiplication());       
        
        
    }
}
