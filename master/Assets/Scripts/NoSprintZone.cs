﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoSprintZone : MonoBehaviour
{
    private Obstacles obstacle;
    [SerializeField] private string compareString = "Player";
    private bool zoneActivated;

    private void Start()
    {
        obstacle = ServiceLocator.obstacleManager;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(compareString))
        {
            zoneActivated = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(compareString))
        {
            zoneActivated = false;
        }
    }

    private void Update()
    {
        if (zoneActivated)
        {
            Vector2 inputVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (Input.GetKey(KeyCode.LeftShift) && inputVector.magnitude != 0)
            {
                if(obstacle != null)
                {
                    obstacle.ChangeHealthValue(1);
                }
                else
                {
                    obstacle = ServiceLocator.obstacleManager;
                }
                
            }
        }
        
    }
}
