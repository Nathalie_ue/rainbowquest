﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperPlane : MonoBehaviour2
{
    bool canBePickedUp;
    [HideInInspector] public GameObject searchParent; 
    Vector3 originalScale, add;
    [HideInInspector] public Rigidbody body;
    [SerializeField] private float additionalGravity;
    Vector3 originalPosition;
    [HideInInspector] public bool isUsedInSearch, wasUsedInSearch;

    private void Awake()            //Awake, because in Start in the Searchable thingy, it will be modified.
    {
        StaticManager.paperPlaneCount++;
        originalPosition = transform.position;
        body = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        originalScale = transform.localScale;        
    }

    private void OnDisable()
    {
        if (!isUsedInSearch)
        {
            StaticManager.currentPaperPlanes.Add(gameObject);
            StaticManager.ChangeCount();
        }
    }

    private void Update()
    {
        if (wasUsedInSearch)
        {
            RayChecks();

            if (transform.localScale != originalScale)
            {
                transform.localScale = SmoothLerp(transform.localScale, originalScale, 0.1f, 0.2f);
            }
        }
    }

    private void FixedUpdate()
    {
        if (wasUsedInSearch)
        {
            if (!grounded) CustomVelocity();
        }
    }


    [SerializeField] float maxSpeed;
    float speed;

    void CustomVelocity()       //"Paperlike" acceleration when in air
    {
        speed = Mathf.Lerp(speed, maxSpeed, 0.5f * TimeMultiplication());
        body.velocity += (transform.right * -speed);
        body.velocity += Vector3.up * -additionalGravity;
    }


    [SerializeField] LayerMask lay;
    bool grounded;
    RaycastHit hit;

    void RayChecks()            //Checking if grounded to be able to fly
    {
        grounded = Physics.Raycast(transform.position, -Vector3.up, out hit, 1, lay);
    }

    private void OnDrawGizmos()
    {
        Vector3 direction = -Vector3.up * 1;
        Gizmos.DrawRay(transform.position, direction);
    }

    bool once;      //restrict
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Player") && !collision.gameObject != searchParent)
        {
            if (!once)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * 0.5f, transform.localScale.z);
                once = true;
            }
            body.constraints = RigidbodyConstraints.FreezeAll;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
