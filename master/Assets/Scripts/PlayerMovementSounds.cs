﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementSounds : MonoBehaviour2
{
    [SerializeField] private float sprintMultiplier = 2;
    [SerializeField, Range(0, 1)] float quantitave_frequency;        //wie häufig/in welchen Abständen
    private float timer;
    [SerializeField] CharacterController controller;

    [SerializeField] AudioSource source;
    [SerializeField] public AudioClip[] woodenSteps, carpetSteps;

    bool wasPlaying;

    public bool canPlay = true;

    private void Start()
    {
        timer = quantitave_frequency;
    }

    private float internSprintMultiplier;
    private void Update()
    {
        if (!Input.GetKey(KeyCode.LeftShift))
        {
            internSprintMultiplier = 1;
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            internSprintMultiplier = sprintMultiplier;
        }

        if (canPlay) PlaySounds();
    }

    void PlaySounds()
    {
        Vector2 playerInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if(controller.velocity != Vector3.zero && controller.isGrounded && playerInput != Vector2.zero)
        {
            if (!wasPlaying)
            {
                PlaySound();
                wasPlaying = true;
            }
            if(timer > 0)
            {
                timer -= Time.deltaTime * internSprintMultiplier;
            }
            else
            {
                PlaySound();
                timer = quantitave_frequency;
            }
        }
        else
        {
            if (wasPlaying)
            {
                PlaySound();
                wasPlaying = false;
            }
        }
    }

    void PlaySound()
    {
        switch (currentMaterial)
        {
            case "Untagged":
                MyPlay(source, woodenSteps[Random.Range(0, woodenSteps.Length)]);
                break;

            case "Carpet":
                MyPlay(source, carpetSteps[Random.Range(0, carpetSteps.Length)]);
                break;
        }
    }

    private void FixedUpdate()
    {
        RayCheck();
    }

    [SerializeField] private LayerMask lay;
    private string currentMaterial;
    private RaycastHit hit;

    void RayCheck()
    {
        Physics.Raycast(transform.position, Vector3.up * -1, out hit, 4, lay);
        if(hit.collider != null)
        {
            if (currentMaterial != hit.collider.tag)
            {
                currentMaterial = hit.collider.tag;
            }
        }
       
    }
}
