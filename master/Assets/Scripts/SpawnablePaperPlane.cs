﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnablePaperPlane : MonoBehaviour2
{
    [SerializeField] private float gravityScale;
    Rigidbody body;

    private void OnEnable()
    {
        if (body == null) body = gameObject.AddComponent<Rigidbody>();
        body.AddForce(transform.forward * 10 * TimeMultiplication(), ForceMode.Impulse);
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.up * -gravityScale * TimeMultiplication());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        body.drag *= 2;
    }
}
