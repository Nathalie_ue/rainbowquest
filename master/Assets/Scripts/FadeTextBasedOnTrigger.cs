﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FadeTextBasedOnTrigger : MonoBehaviour2
{
    [SerializeField] private TextMeshProUGUI text;
    private Color originalColor, noOpacityColor;
    private float opacity;
    private bool lerpTicket;

    private void Start()
    {
        originalColor = text.color;
        noOpacityColor = new Color(originalColor.r, originalColor.g, originalColor.b, 0);
        text.color = noOpacityColor;
    }

    private void Update()
    {
        if (lerpTicket)
        {
            if(text.color != noOpacityColor)
            {
                text.color = Color.Lerp(text.color, noOpacityColor, 0.1f * TimeMultiplication());
            }
            else
            {
                lerpTicket = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            text.color = originalColor;
            lerpTicket = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            lerpTicket = true;
        }
    }
}
