﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAudioManager : MonoBehaviour2
{
    [SerializeField] private AudioSource source;
    float originalVolume;
    private void Start()
    {
        originalVolume = source.volume;
    }
    public void PlaySoundEffect(AudioClip clip)
    {
        source.volume = originalVolume;
        MyPlay(source, clip);
    }

    public void PlaySoundEffectLouder(AudioClip clip)
    {
        source.volume = originalVolume + 0.3f;
        MyPlay(source, clip);
    }
}
