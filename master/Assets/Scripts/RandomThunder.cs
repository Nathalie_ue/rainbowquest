﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomThunder : MonoBehaviour2
{
    [SerializeField] AudioClip thunder;
    [SerializeField] AudioSource audioSource;
    [SerializeField] float maxTimer;
    float timer;

    private void Start()
    {
        timer = maxTimer;
    }

    private void Update()
    {
        if(!audioSource.isPlaying)
        {
            if(timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                MyPlay(audioSource, thunder);
                timer = maxTimer * Random.Range(0.9f, 1.1f);
            }
        }
    }
}
