﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScaleOnTrigger : MonoBehaviour2
{
    [SerializeField] private Vector3 onTriggerVector = new Vector3(1, 1, 1);
    private Vector3 originalScale;
    private bool lerpToOriginalTicket;

    private void Start()
    {
        originalScale = transform.localScale;
    }

    private void Update()
    {
        if (lerpToOriginalTicket)
        {
            if(transform.localScale != originalScale)
            {
                transform.localScale = SmoothLerp(transform.localScale, originalScale, 0.1f, 0.1f * TimeMultiplication());
            }
            else
            {
                lerpToOriginalTicket = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
         transform.localScale = onTriggerVector;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            lerpToOriginalTicket = true;
        }
    }
}
