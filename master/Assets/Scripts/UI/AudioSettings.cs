﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class AudioSettings : MonoBehaviour
{
    public Slider backgroundMusicSlider, soundEffectsSlider;
    public AudioSource backgroundAudio;

    public List<AudioSource> soundEffectsAudio = new List<AudioSource>();
    private float backgroundFloat, soundEffectsFloat;
    
    private void Awake()
    {
        
    }

    private void Start()
    {
        //Audios der Szene werden gesucht
        soundEffectsAudio.AddRange(FindObjectsOfType<AudioSource>());

        for (int i = 0; i < soundEffectsAudio.Count; i++)
        {
            if (soundEffectsAudio[i].CompareTag("BackgroundMusic"))
            {
                backgroundAudio = soundEffectsAudio[i];
                soundEffectsAudio.RemoveAt(i);
            }
        }

        //Die Einstellungen der letzten Szene werden übernommen
        ContinueSettings();
    }

    private void ContinueSettings()
    {
        backgroundFloat = PlayerPrefs.GetFloat("BackgroundMusicVolume");
        soundEffectsFloat = PlayerPrefs.GetFloat("SoundEffectVolume");

        //Lautstärke wird zugewiesen und Slider angepasst
        backgroundAudio.volume = backgroundFloat;
        backgroundMusicSlider.value = backgroundFloat;

        for (int i = 0; i < soundEffectsAudio.Count; i++)
        {
            soundEffectsAudio[i].volume = soundEffectsFloat;
            soundEffectsSlider.value = soundEffectsFloat;
        }
    }
}
