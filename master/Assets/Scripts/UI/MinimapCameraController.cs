﻿using UnityEngine;
using System.Collections;

public class MinimapCameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private Vector3 downstairsPosition;

    [SerializeField]
    private Vector3 upstairsPosition;
    
    private void FixedUpdate()
    {
        if(player.transform.position.y > 50)
        {
            BringCameraUpstairs();
        }
        else
        {
            BringCameraDownstairs();
        }
    }

    private void BringCameraUpstairs()
    {
        transform.position = upstairsPosition;
    }

    private void BringCameraDownstairs()
    {
        transform.position = downstairsPosition;
    }
}
