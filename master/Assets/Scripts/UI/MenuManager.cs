﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{

    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip enablePause, disablePause;
    
    [SerializeField]
    private GameObject settingMenu;

    public float camMovementTime;
    
    public GameObject menuFirstButton, optionsFirstButton, optionsClosedButton;

    public Vector3 defaultPosition;
    public Vector3 settingsPosition;

    public Quaternion defaultRotation;
    public Quaternion settingRotation;

    public Vector3 menuButtonDefault;
    public Vector3 menuButtonMoveTo;


    public GameObject mainCamera;
    public Transform menuButtons;
    public CanvasGroup settingButtons;

    [Range (0f, 1f)]
    public float movementSpeed;

    [SerializeField]
    private float floatSpeed;
    
    [SerializeField]
    private float floatHight;
    
    private float originalYposition;

    public bool notMovingCamera;

    void Start()
    {
        originalYposition = defaultPosition.y;
        notMovingCamera = true;

        //clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(menuFirstButton);
    }    

    private void Update()
    {
        if (notMovingCamera)
        {
            mainCamera.transform.localPosition = new Vector3(mainCamera.transform.localPosition.x, originalYposition + ((float)Mathf.Sin(Time.realtimeSinceStartup * floatSpeed) * floatHight), mainCamera.transform.localPosition.z);
        }
    }

    public void OpenSettings()
    {
        notMovingCamera = false;

        StartCoroutine(ZoomIn());
    }

    public void CloseSettings()
    {
        //notMovingCamera = false;

        StartCoroutine(ZoomBack());
    }

    private IEnumerator ZoomIn()
    {
        float startTime = Time.time;
        while (Time.time < startTime + movementSpeed)
        {
            mainCamera.transform.position = Vector3.Lerp(defaultPosition, settingsPosition, (Time.time - startTime) / movementSpeed);
            mainCamera.transform.rotation = Quaternion.Lerp(defaultRotation, settingRotation, (Time.time - startTime) / movementSpeed);
                        
            menuButtons.transform.localPosition = Vector3.Lerp(menuButtonDefault, menuButtonMoveTo, (Time.time - startTime) / movementSpeed);
            yield return null;
        }
        mainCamera.transform.position = settingsPosition;
        mainCamera.transform.rotation = settingRotation;

        menuButtons.transform.localPosition = menuButtonMoveTo;

        settingMenu.SetActive(true);


        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(optionsFirstButton);

        //originalYposition = settingsPosition.y;
        //notMovingCamera = true;
    }

    IEnumerator ZoomBack()
    {
        settingMenu.SetActive(false);

        float startTime = Time.time;
        while (Time.time < startTime + movementSpeed)
        {
            mainCamera.transform.position = Vector3.Lerp(settingsPosition, defaultPosition, (Time.time - startTime) / movementSpeed);
            mainCamera.transform.rotation = Quaternion.Lerp(settingRotation, defaultRotation, (Time.time - startTime) / movementSpeed);

            menuButtons.transform.localPosition = Vector3.Lerp(menuButtonMoveTo, menuButtonDefault, (Time.time - startTime) / movementSpeed);
            yield return null;
        }
        mainCamera.transform.position = defaultPosition;
        mainCamera.transform.rotation = defaultRotation;

        menuButtons.transform.localPosition = menuButtonDefault;


        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(optionsClosedButton);

        originalYposition = defaultPosition.y;
        notMovingCamera = true;
    }
}