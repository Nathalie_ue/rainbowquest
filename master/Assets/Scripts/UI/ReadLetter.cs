﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class ReadLetter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI closedLetterText;

    public GameObject openedLetter;

    [SerializeField]
    private Sprite openLetterImage;
    private Image letterImage;

    [SerializeField]
    private LetterSceneManager letterSceneManager;
    public int nightNumber;
    public bool isOldLetter;

    private void Start()
    {
        letterImage = this.GetComponent<Image>();
                
        letterSceneManager.AddLetterToList(this.gameObject);
    }

    //Brief wird geöffnet
    public void OpenLetter()
    {              
        letterImage.sprite = openLetterImage;

        openedLetter.SetActive(true);
    }

    public void CloseLetter()
    {
        openedLetter.SetActive(false);
        //if (closedLetterText != null)
        //{
        //    closedLetterText.text = "Nochmal lesen";
        //}
    }
}