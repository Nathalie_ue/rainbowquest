﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class SettingMenu : MonoBehaviour
{
    public Slider backgroundMusicSlider, soundEffectsSlider;

    //public AudioSettings audioSettings;
    public AudioSource backgroundAudio;
    public List<AudioSource> soundEffectsAudio = new List<AudioSource>();

    private float backgroundFloat, soundEffectsFloat;
    
    public bool mainMenu;
        
    private void Start()
    {
        PlayerPrefs.SetFloat("BackgroundMusicVolume", backgroundFloat);
        PlayerPrefs.SetFloat("SoundEffectVolume", soundEffectsFloat);

        //Audios der Szene werden gesucht und zugewiesen
        soundEffectsAudio.AddRange(FindObjectsOfType<AudioSource>());

        for (int i = 0; i < soundEffectsAudio.Count; i++)
        {
            if (soundEffectsAudio[i].CompareTag("BackgroundMusic"))
            {
                backgroundAudio = soundEffectsAudio[i];
                soundEffectsAudio.RemoveAt(i);
            }
        }

        if(mainMenu)
        {
            backgroundAudio.volume = 0.129f;
            backgroundAudio.volume = backgroundMusicSlider.value;

            for (int i = 0; i < soundEffectsAudio.Count; i++)
            {
                soundEffectsAudio[i].volume = 0.129f;
                soundEffectsAudio[i].volume = soundEffectsSlider.value;
            }

            SaveSoundSettings();
        }
    }

    public void SetVolume()
    {
        //Lautstärke wird verändert
        backgroundAudio.volume = backgroundMusicSlider.value;

        for (int i = 0; i < soundEffectsAudio.Count; i++)
        {
            soundEffectsAudio[i].volume = soundEffectsSlider.value;
        }
    }

    public void OnContinue()
    {
        SaveSoundSettings();
    }

    public void SaveSoundSettings()
    {
        //Einstellungen werden gespeichert
        PlayerPrefs.SetFloat("BackgroundMusicVolume", backgroundMusicSlider.value);
        PlayerPrefs.SetFloat("SoundEffectVolume", soundEffectsSlider.value);
    }
}
