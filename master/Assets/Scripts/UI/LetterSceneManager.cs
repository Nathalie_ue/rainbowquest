﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class LetterSceneManager : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip enablePause, disablePause;

    public GameObject menuFirstButton;

    public GameObject nightOneButton, nightTwoButton, nightThreeButton, nightFourButton;

    private bool lettersMissing = true;
    private bool letterClicked = false;
      
    public static int levelCount = 4;

    [SerializeField]
    private List<GameObject> letterList = new List<GameObject>();

    public LevelLoader levelLoader;

    [SerializeField] private string[] levelNames;       //I used a string array for level-loading refence

    private void Start()
    {
        Debug.Log(gameObject);
        if (levelCount == 1)
        {
            levelLoader.sceneToLoad = levelNames[levelCount - 1];           //appropriate level will be set as the string to laod
            menuFirstButton = nightOneButton;
        }
        else if (levelCount == 2)
        {
            levelLoader.sceneToLoad = levelNames[levelCount - 1];
            menuFirstButton = nightTwoButton;
        }
        else if (levelCount == 3)
        {
            levelLoader.sceneToLoad = levelNames[levelCount - 1];
            menuFirstButton = nightThreeButton;
        }
        else if (levelCount == 4)
        {
            levelLoader.sceneToLoad = levelNames[levelCount - 1];
            menuFirstButton = nightFourButton;
        }


        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(menuFirstButton);
    }

    public void AddLetterToList(GameObject letter)
    {
        // We use the .Add method to add items to a list
        letterList.Add(letter);

        //all letters get deactivated
        letter.SetActive(false);
    }

    private void Update()
    {
        ShowNeededLetters();
    }

    private void ShowNeededLetters()
    {
        //Check if letters are missing to stop the loop if there aren't any missing
        if (lettersMissing)
        {
            foreach (GameObject letter in letterList)
            {
                //Check the number in the letters with the current level to activate and deactivate the correct ones
                if (letter.gameObject.GetComponent<ReadLetter>().nightNumber == levelCount)
                {
                    letter.SetActive(true);
                }
                else
                {
                    letter.SetActive(false);
                }

                //If the letters are on the "old bar" they don't get deactivated
                if (letter.gameObject.GetComponent<ReadLetter>().isOldLetter)
                {
                    if (letter.gameObject.GetComponent<ReadLetter>().nightNumber <= levelCount)
                    {
                        letter.SetActive(true);
                    }
                }
            }

            lettersMissing = false;
        }
    }
}
