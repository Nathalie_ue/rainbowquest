﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour2
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip enablePause, disablePause;

    private bool GameIsPaused = false;
    private bool SecondMenuIsClosed = true;

    [SerializeField]
    private GameObject pauseMenu;

    [SerializeField]
    private GameObject minimap;

    [SerializeField]
    private GameObject settingMenu, controlsMenu;

    [SerializeField]
    private Toggle minimapToggle;

    private PlayerMovement freezeRotation;
    public GameObject player;

    public GameObject pauseFirstButton, optionsFirstButton, optionsClosedButton, controlsButton, controlsClosedButton;


    private void Start()
    {
        Cursor.visible = false;         //starting cursor modifications here
        freezeRotation = player.GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused && SecondMenuIsClosed)
            {
                MyPlay(audioSource, disablePause, 1, 0.1f, 0, false, false);        //No Idea why i have to use the more complicated one
                Resume();                                                           // but if I dont, the Pitch will be set to 0                
            }
            else
            {
                MyPlay(audioSource, enablePause);
                Pause();
                
            }
        }
    }

    public void Resume()
    {
        Cursor.visible = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        freezeRotation.useWobblyBackRotation = true;
    }

    private void Pause()
    {
        Cursor.visible = true;
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        freezeRotation.useWobblyBackRotation = false;

        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(pauseFirstButton);
    }

    public void OpenOptions()
    {
        settingMenu.SetActive(true);
        SecondMenuIsClosed = false;

        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(optionsFirstButton);
    }

    public void CloseOptions()
    {
        settingMenu.SetActive(false);
        SecondMenuIsClosed = true;

        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(optionsClosedButton);
    }

    public void OpenControls()
    {
        controlsMenu.SetActive(true);
        SecondMenuIsClosed = false;

        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(controlsButton);
    }

    public void CloseControls()
    {
        controlsMenu.SetActive(false);
        SecondMenuIsClosed = true;

        ////clear the selected object of the event system
        //EventSystem.current.SetSelectedGameObject(null);
        ////Set a new selected object
        //EventSystem.current.SetSelectedGameObject(controlsClosedButton);
    }

    public void MinimapOnScreen()
    {
        if (minimapToggle.isOn)
        {
            minimap.SetActive(true);
        }
        else
        {
            minimap.SetActive(false);
        }
    }
}
