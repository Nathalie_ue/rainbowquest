﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CreditManager : MonoBehaviour2
{
    [SerializeField]
    private GameObject mainCamera;
    [SerializeField]
    private float floatSpeed;
    [SerializeField]
    private float floatHight;
    private float originalYposition;

    [SerializeField]
    private GameObject firstLetter, secondLetter, thirdLetter, fourthLetter, fifthLetter;
    [SerializeField]
    private TextMeshProUGUI titleText;
    
    [SerializeField]
    private GameObject paperplaneObject, lightObject, cameronObject, dadObject, dogObject, momObject, pointOne, pointTwo, pointThree,startKI, stopKI, endKI;
    [SerializeField]
    private Vector3 startPosition, startPositionMiddle, onScreenPosition, onScreenPositionMiddle, endPosition, endPositionMiddle;

    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip paperClip, dogClip, momClip;
    

    [SerializeField] private Image backgroundImage;
    bool lerpImage;
    [SerializeField]
    private float normalLerpSpeed, planeSpeed = 2f;
    Color nonTransparentColor, nonTransparentText;
    public Color lightBackground, transparentColor;

    private float originalSourceVolume;     //saving AS volume to reference it later on.

    private bool speedUp = false;
    [SerializeField]
    private TextMeshProUGUI buttonText;

    private void Start()
    {
        originalYposition = mainCamera.transform.localPosition.y;
        nonTransparentColor = backgroundImage.color;

        nonTransparentText = titleText.color;
        titleText.color = transparentColor;
                
        lerpImage = true;
    }

    bool endGameTicket;

    private void Update()
    {
        mainCamera.transform.localPosition = new Vector3(mainCamera.transform.localPosition.x, originalYposition + ((float)Mathf.Sin(Time.realtimeSinceStartup * floatSpeed) * floatHight), mainCamera.transform.localPosition.z);

        if (lerpImage)
        {
            if (!endGameTicket)
            {
                StartCoroutine(EndGameCoroutine());
                endGameTicket = true;
            }
        }
    }

    public void CreditSpeed()
    {
        if(!speedUp)
        {
            normalLerpSpeed = 1f;
            buttonText.text = "Langsamer";
            speedUp = true;
        }
        else
        {
            normalLerpSpeed = 3.5f;
            buttonText.text = "Schneller";
            speedUp = false;
        }
    }

    IEnumerator EndGameCoroutine()
    {
        //Schwarzer Screen fadeout
        float startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            backgroundImage.color = Color.Lerp(nonTransparentColor, transparentColor, (Time.time - startTime) / normalLerpSpeed);
            yield return null;
        }
        backgroundImage.color = transparentColor;

        //Papierflieger durch den Raum fliegen lassen 
        startTime = Time.time;
        while (Time.time < startTime + planeSpeed)
        {
            paperplaneObject.transform.localPosition = Vector3.Lerp(pointOne.transform.localPosition, pointTwo.transform.localPosition, (Time.time - startTime) / planeSpeed);
            paperplaneObject.transform.localRotation = Quaternion.Lerp(pointOne.transform.localRotation, pointTwo.transform.localRotation, (Time.time - startTime) / planeSpeed);

            yield return null;
        }
        startTime = Time.time;
        while (Time.time < startTime + planeSpeed)
        {
            paperplaneObject.transform.localPosition = Vector3.Lerp(pointTwo.transform.localPosition, pointThree.transform.localPosition, (Time.time - startTime) / planeSpeed);
            paperplaneObject.transform.localRotation = Quaternion.Lerp(pointTwo.transform.localRotation, pointThree.transform.localRotation, (Time.time - startTime) / planeSpeed);

            yield return null;
        }
        paperplaneObject.transform.localPosition = pointThree.transform.localPosition;
        paperplaneObject.transform.localRotation = pointThree.transform.localRotation;

        ////Titel einblenden 
        startTime = Time.time;
        while (Time.time < startTime + planeSpeed)
        {
            titleText.color = Color.Lerp(transparentColor, nonTransparentText, (Time.time - startTime) / planeSpeed);
            yield return null;
        }
        titleText.color = nonTransparentText;

        yield return new WaitForSecondsRealtime(1.2f);

        //Titel nach oben 
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            titleText.transform.localPosition = Vector3.Lerp(onScreenPositionMiddle, endPositionMiddle, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        titleText.transform.localPosition = endPositionMiddle;

        //ersten Brief nach oben, Cameron kommt ins Zimmer
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            firstLetter.transform.localPosition = Vector3.Lerp(startPosition, onScreenPosition, (Time.time - startTime) / normalLerpSpeed);
            cameronObject.transform.localPosition = Vector3.Lerp(startKI.transform.localPosition, stopKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        firstLetter.transform.localPosition = onScreenPosition;
        cameronObject.transform.localPosition = stopKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(2f);

        ////Brief verschwindet, als ob Cameron ihn eingesammelt hat, Papiersound spielt        
        audioSource.clip = paperClip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f)) * Time.timeScale;
        audioSource.volume = 1f;
        audioSource.Play();
        paperplaneObject.SetActive(false);

        yield return new WaitForSecondsRealtime(2f);

        //Cameron geht, erster Brief geht nach oben
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            firstLetter.transform.localPosition = Vector3.Lerp(onScreenPosition, endPosition, (Time.time - startTime) / normalLerpSpeed);
            cameronObject.transform.localPosition = Vector3.Lerp(stopKI.transform.localPosition, endKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        firstLetter.transform.localPosition = endPosition;
        cameronObject.transform.localPosition = endKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(0.5f);

        ////zweiter Brief geht nach oben, Dad kommt rein
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            secondLetter.transform.localPosition = Vector3.Lerp(startPosition, onScreenPosition, (Time.time - startTime) / normalLerpSpeed);
            dadObject.transform.localPosition = Vector3.Lerp(startKI.transform.localPosition, stopKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        secondLetter.transform.localPosition = onScreenPosition;
        dadObject.transform.localPosition = stopKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(2f);

        lightObject.SetActive(true);

        yield return new WaitForSecondsRealtime(2f);

        ////Dad geht, zweiter Brief geht nach oben
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            secondLetter.transform.localPosition = Vector3.Lerp(onScreenPosition, endPosition, (Time.time - startTime) / normalLerpSpeed);
            dadObject.transform.localPosition = Vector3.Lerp(stopKI.transform.localPosition, endKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        secondLetter.transform.localPosition = endPosition;
        dadObject.transform.localPosition = endKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(0.5f);

        ////dritter Brief geht nach oben, Hund kommt rein
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            thirdLetter.transform.localPosition = Vector3.Lerp(startPosition, onScreenPosition, (Time.time - startTime) / normalLerpSpeed);
            dogObject.transform.localPosition = Vector3.Lerp(startKI.transform.localPosition, stopKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        thirdLetter.transform.localPosition = onScreenPosition;
        dogObject.transform.localPosition = stopKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(2f);

        ////Hund bellt
        audioSource.clip = dogClip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f)) * Time.timeScale;
        audioSource.volume = 0.05f;
        audioSource.Play();

        yield return new WaitForSecondsRealtime(2f);

        ////Hund geht, dritter Brief geht nach oben
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            thirdLetter.transform.localPosition = Vector3.Lerp(onScreenPosition, endPosition, (Time.time - startTime) / normalLerpSpeed);
            dogObject.transform.localPosition = Vector3.Lerp(stopKI.transform.localPosition, endKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        thirdLetter.transform.localPosition = endPosition;
        dogObject.transform.localPosition = endKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(0.5f);

        ////vierter Brief geht nach oben, Mom kommt rein
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            fourthLetter.transform.localPosition = Vector3.Lerp(startPosition, onScreenPosition, (Time.time - startTime) / normalLerpSpeed);
            momObject.transform.localPosition = Vector3.Lerp(startKI.transform.localPosition, stopKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        fourthLetter.transform.localPosition = onScreenPosition;
        momObject.transform.localPosition = stopKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(2f);

        ////Mom goes "HUUUHH"
        audioSource.clip = momClip;
        audioSource.pitch = (Random.Range(0.9f, 1.1f)) * Time.timeScale;
        audioSource.volume = 0.05f;
        audioSource.Play();

        yield return new WaitForSecondsRealtime(2f);

        ////Licht geht aus, Mom geht, vierter Brief geht nach oben
        lightObject.SetActive(false);
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            fourthLetter.transform.localPosition = Vector3.Lerp(onScreenPosition, endPosition, (Time.time - startTime) / normalLerpSpeed);
            momObject.transform.localPosition = Vector3.Lerp(stopKI.transform.localPosition, endKI.transform.localPosition, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        fourthLetter.transform.localPosition = endPosition;
        momObject.transform.localPosition = endKI.transform.localPosition;

        yield return new WaitForSecondsRealtime(0.5f);

        ////fünfter Brief geht nach oben
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            fifthLetter.transform.localPosition = Vector3.Lerp(startPositionMiddle, onScreenPositionMiddle, (Time.time - startTime) / normalLerpSpeed);

            yield return null;
        }
        fifthLetter.transform.localPosition = onScreenPositionMiddle;

        yield return new WaitForSecondsRealtime(2f);

        //Screen wird schwarz
        startTime = Time.time;
        while (Time.time < startTime + normalLerpSpeed)
        {
            backgroundImage.color = Color.Lerp(transparentColor, nonTransparentColor, (Time.time - startTime) / normalLerpSpeed);
            yield return null;
        }
        backgroundImage.color = nonTransparentColor;

        //Szene wechselt sich       
        SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);              
    }
}
