﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

//Script, dass das Spiel startet
public class LevelLoader : MonoBehaviour2       //Inheriting from my custom script to use SetTime Method
{
    //Szene, die geladen werden soll, wird über den Inspektor eingetragen
    public string sceneToLoad;

    //Variabel für den button
    private Button button;

    private void Awake()
    {
        //Die Komponente wird der Variabel zugeschrieben
        button = GetComponent<Button>();
    }

    //Immer wenn das Object aktiviert wird
    private void OnEnable()
    {
        //onClick des Buttons über Script
        button.onClick.AddListener(LoadScene);
    }

    //Immer wenn ein Object deaktiviert wird
    private void OnDisable()
    {
        button.onClick.RemoveListener(LoadScene);
    }

    //Szene in sceneToLoad wird geladen
    //Sceneloading implies the reset of the Timescale, thus i reset it here
    public void LoadScene()
    {
        SetTime(1);     //resetting Timescale 
        SceneManager.LoadScene(sceneToLoad);
    }
}

